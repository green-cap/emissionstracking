import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
 class GreenCapScheduler {
  

  static void catchUpWholeSchedule() async {
        Future<SharedPreferences> _prefs =  SharedPreferences.getInstance();
        final SharedPreferences prefs = await _prefs;

        bool logged = prefs.getBool("isLoggedIn");
        if (logged == null || logged == false) {
          return;
        }

        DatabaseReference db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));
        DatabaseReference sched = db.child("schedule");
        var events = await sched.once();

        if (events == null) {
          return;
        }
        //At this point schedule exists and they are logged in 
        DateTime _date = new DateTime.now();
        DateTime today = new DateTime.utc( _date.year,_date.month, _date.day);

        //loop through all scheduled events
        if (events.value != null) {
          for (var k in events.value.keys) {
              //dont want to process the id parent
              if (k == "id" || k == null) {
                continue;
              }
              // print("----------------------------------");
              // print(k);
            if (events.value[k]["stop"] != null && events.value[k]["stop"]  == true) {
                continue;
            }

              var initialDate = DateFormat('dd-MM-yyyy').parse(events.value[k]["lastLogged"]);
              int freq = events.value[k]["freq"];
              String device = events.value[k]["device"];
              int deviceId = events.value[k]["deviceId"];
              int duration = events.value[k]["duration"];
              

              DatabaseReference instances = sched.child(k).child("instances");

              initialDate = initialDate.add(new Duration(days: freq));
              
              var lastLogged = initialDate;
              bool added = false;


              while(initialDate.isBefore(today)) {
                  String strDate = formatAnyDate(initialDate);
                  lastLogged = initialDate;
                  added = true;
                  
                  Map mp = {
                    "device": device,
                    "duration": duration ,
                    "scheduled": true,
                    "deviceId" : deviceId,
                    "sid": int.parse(k),
                  };

                  var typeRef = db.child(strDate).child(events.value[k]["type"]);
                  DataSnapshot ds = await typeRef.once();
                  int idA = 0;

                  if (ds.value == null) {
                    typeRef.child("0").set(mp);
                    typeRef.update({"id" : 1});
                    
                  } else {
                    typeRef.child(ds.value['id'].toString()).set(mp);
                    typeRef.update({"id": ds.value['id'] + 1});
                    idA = ds.value['id'];
                  }

                  instances.child(strDate).set(idA);
                  initialDate = initialDate.add(new Duration(days: freq));
              }



              if (added) {
                  sched.child(k).update({"lastLogged":formatAnyDate(lastLogged)});
              }

            }
        } 
        
    }
    
  static String formatAnyDate(DateTime _date) {
      return "${_date.day.toString().padLeft(2, '0')}-${_date.month.toString().padLeft(2, '0')}-${_date.year.toString()}";
  }

}

