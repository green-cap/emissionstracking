import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:greencap/models/models.dart';

class AddEmissionsBubble extends StatefulWidget {
  final String type;
  final Icon icon;
  final Color bgColour;
  final String date;
  final DataSnapshot ds;
  final DataSnapshot devices;
  final String hintText;
  final DatabaseReference db;
  final bool food;
  AddEmissionsBubble(this.type, this.icon, this.bgColour, this.date, this.ds,
      this.hintText, this.db, this.food, this.devices);

  @override
  _AddEmissionsBubbleState createState() => _AddEmissionsBubbleState();
}

class _AddEmissionsBubbleState extends State<AddEmissionsBubble> {
  final TextStyle title = TextStyle(
    fontFamily: "Avenir",
    fontWeight: FontWeight.w500,
    fontSize: 25,
    decoration: TextDecoration.underline,
    color: Colors.white,
  );

  final TextStyle tableHeadStyle = TextStyle(
    fontFamily: "Avenir",
    fontWeight: FontWeight.w500,
    fontSize: 15,
    decoration: TextDecoration.underline,
    color: Colors.white,
  );

  final TextStyle tableRowStyle = TextStyle(
    fontFamily: "Avenir",
    fontWeight: FontWeight.w500,
    fontSize: 15,
    color: Colors.white,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: widget.bgColour,
        ),
        child: SingleChildScrollView(
            child: Column(children: [
          // Electricity label, icon and add button
          Row(children: [
            Padding(
                child: Text(widget.type, style: title),
                padding: EdgeInsets.all(5.0)),
            widget.icon,
            Spacer(),
            RaisedButton(
              color: Color(0xFF05CC7F),
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: BorderSide(color: Colors.green)),
              onPressed: () {
                //Try refactor this // basically repeating 3 of the same widget
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      DeviceK device = null;
                      TextEditingController duration = TextEditingController();

                      return Dialog(
                          child: Expanded(
                        child: Container(
                            padding: EdgeInsets.all(15),
                            height: 400,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("Add " + widget.type + "\n\n"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: DropdownSearch<DeviceK>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showSelectedItem: false,
                                          showSearchBox: true,
                                          itemAsString: (DeviceK d) => d.name,
                                          items: generateSearchItems(context),
                                          onChanged: (DeviceK data) =>
                                              device = data,
                                          label:
                                              widget.food ? 'Food' : 'Device'),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: new TextField(
                                        autofocus: true,
                                        keyboardType: TextInputType.number,
                                        controller: duration,
                                        decoration: new InputDecoration(
                                            labelText: widget.food
                                                ? 'Amount(grams)'
                                                : 'Duration(mins)',
                                            hintText: 'e.g. 120'),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    RaisedButton(
                                      child: Text("Cancel"),
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                    ),
                                    Spacer(),
                                    RaisedButton(
                                      child: Text("Add"),
                                      onPressed: () async {
                                        if (device == null ||
                                            duration.text == "") {
                                          return;
                                        }

                                        addRow(device.name, duration.text,
                                            device.id);
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                )
                              ],
                            )),
                      ));
                    });
              },
              child: Text("ADD",
                  style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 1.5,
                      fontFamily: "Avenir",
                      fontWeight: FontWeight.w900,
                      fontSize: 25)),
            )
          ]),
          //DataTable wrapped up in a row to ake it expand to size.
          Row(// a dirty trick to make the DataTable fit width
              children: <Widget>[
            Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: generateTable(context)))
          ]),
        ])));
  }

  List<DeviceK> generateSearchItems(BuildContext context) {
    var devices = widget.devices.value[widget.type];
    List<DeviceK> items = [];
    for (var key in devices.keys) {
      if (key == 'id') {
        continue;
      }

      items.add(DeviceK(id: int.parse(key), name: devices[key]['name']));
    }
    return items;
  }

  void addRow(String device, String duration, int dId) async {
    DatabaseReference dbRef = widget.db.child(widget.date).child(widget.type);
    DataSnapshot nullChecker = await dbRef.once();

    if (nullChecker.value == null) {
      Map mp = {
        "device": device,
        "deviceId": dId,
        "duration": int.parse(duration),
        "scheduled": false
      };
      dbRef.child("0").set(mp);
      dbRef.update({"id": 1});
    } else {
      Map mp = {
        "device": device,
        "deviceId": dId,
        "duration": int.parse(duration),
        "scheduled": false
      };
      dbRef.child(nullChecker.value["id"].toString()).set(mp);
      dbRef.update({"id": nullChecker.value["id"] + 1});
    }
  }

  void editRowDialog(String date, String type, String id, String deviceS,
      String dur, bool scheduled) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          TextEditingController device = TextEditingController();
          TextEditingController duration = TextEditingController();

          device.text = deviceS;
          duration.text = dur;

          return Dialog(
              child: Container(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Edit " + widget.type),
                      Row(
                        children: [
                          Expanded(
                            child: new TextField(
                              autofocus: true,
                              controller: device,
                              decoration: new InputDecoration(
                                  labelText: widget.food ? 'Food' : 'Device',
                                  hintText: ''),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: new TextField(
                              autofocus: true,
                              keyboardType: TextInputType.number,
                              controller: duration,
                              decoration: new InputDecoration(
                                  labelText: 'Duration', hintText: 'e.g. 5'),
                            ),
                          ),
                        ],
                      ),
                      // Row(
                      //   children: [
                      //     Expanded(
                      //       child: new TextField(
                      //         controller: repetition,
                      //         autofocus: true,
                      //         decoration: new InputDecoration(
                      //             labelText: 'Reptition',
                      //             hintText: 'e.g.every day'),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      RaisedButton(
                        child: Text("Update"),
                        onPressed: () async {
                          //Invariant: You'll only be able to edit objects that exist
                          // So do no need to do checking.

                          editScheduledCheck(date, type, id, device.text,
                              duration.text, scheduled);
                        },
                      )
                    ],
                  )));
        });
  }

  void editScheduledCheck(String date, String type, String id, String device,
      String duration, bool scheduled) {
    if (scheduled) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text("Edit all instances of this scheduled event? "),
              actions: [
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () {
                    editScheduled(date, type, id, device, duration);
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('No, just this one'),
                  onPressed: () {
                    editRow(date, type, id, device, duration);
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      editRow(date, type, id, device, duration);
      Navigator.of(context).pop();
    }
  }

  void editScheduled(String date, String type, String id, String device,
      String duration) async {
    var eventParsed = await widget.db.child(date).child(type).child(id).once();
    int sid = eventParsed.value["sid"];
    String sidStr = sid.toString();

    var instances =
        widget.db.child("schedule").child(sidStr).child("instances").once();
    var instancesGotten = await instances;

    instancesGotten.value
        .forEach((k, v) => {editRow(k, type, v.toString(), device, duration)});
  }

  void editRow(
      String date, String type, String id, String device, String duration) {
    DatabaseReference dbRef = widget.db.child(date).child(type).child(id);
    dbRef.update({"device": device, "duration": int.parse(duration)});
  }

  List<Widget> divider() {
    return [
      Divider(color: Colors.white),
      Divider(color: Colors.white),
      Divider(color: Colors.white),
      Divider(color: Colors.white),
      Divider(color: Colors.white)
    ];
  }

  Widget generateTable(BuildContext bc) {
    DataSnapshot ds = widget.ds;
    String date = widget.date;

    List<TableRow> dr = new List();
    dr.add(TableRow(children: [
      Text(""),
      Text(""),
      Text(widget.food ? 'Food' : 'Device', style: tableHeadStyle),
      Text(widget.food ? 'Amount' : 'Duration', style: tableHeadStyle),
      Text("Scheduled", style: tableHeadStyle)
    ]));

    if (ds == null || ds.value == null) {
      dr.add(TableRow(children: [
        Text(""),
        Text(""),
        Text("", style: tableHeadStyle),
        Text("", style: tableHeadStyle),
        Text("", style: tableHeadStyle)
      ]));
      return Table(
        children: dr,
        columnWidths: {0: FractionColumnWidth(0.1)},
      );
    }

    var deep = ds.value[date];
    if (deep != null) {
      var deep2 = deep[widget.type];

      if (deep2 != null) {
        if (deep2.keys == null) {
          print(deep2);
          print(date);
        }
        for (var k in deep2.keys) {
          String l = k.toString();
          if (l == "id" || l == null) {
            continue;
          }

          String scheduleString = "";
          bool isScheduled = false;
          var scheduled = deep2[k]["scheduled"];
          if (scheduled == null) {
            scheduleString = "No";
            isScheduled = false;
          } else {
            if (scheduled == false) {
              scheduleString = "No";
              isScheduled = false;
            } else {
              scheduleString = "Yes";
              isScheduled = true;
            }
          }

          dr.add(TableRow(children: [
            Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: GestureDetector(
                    onTap: () async {
                      deleteScheduledCheck(
                          widget.date, widget.type, l, isScheduled);
                    },
                    child: Icon(
                      Icons.delete_forever,
                      color: Colors.white,
                      size: 27,
                    ))),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: GestureDetector(
                    onTap: () async {
                      editRowDialog(
                          date,
                          widget.type,
                          l,
                          deep[widget.type][k]["device"],
                          deep[widget.type][k]["duration"].toString(),
                          isScheduled);
                    },
                    child: Icon(
                      Icons.edit,
                      size: 27,
                      color: Colors.white,
                    ))),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: Text(
                  deep[widget.type][k]["device"],
                  style: tableRowStyle,
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: Text(
                  deep[widget.type][k]["duration"].toString(),
                  style: tableRowStyle,
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: Text(
                  scheduleString,
                  style: tableRowStyle,
                ))
          ]));
          dr.add(TableRow(children: divider()));
        }
      }
    }

    if (dr.length == 1) {
      //Add an empty row for padding
      dr.add(TableRow(children: [
        Text(""),
        Text(""),
        Text("", style: tableHeadStyle),
        Text("", style: tableHeadStyle),
        Text("", style: tableHeadStyle)
      ]));
    }

    Table dt = Table(
      children: dr,
      columnWidths: {0: FractionColumnWidth(0.1), 1: FractionColumnWidth(0.1)},
    );

    return dt;
  }

  void deleteScheduledCheck(
      String date, String type, String id, bool scheduled) {
    if (scheduled) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text("Delete all instances of this scheduled event? "),
              actions: [
                FlatButton(
                  child: Text('Yes, and stop adding'),
                  onPressed: () {
                    deleteScheduled(date, type, id, true, false);
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('Yes, but keep adding'),
                  onPressed: () {
                    deleteScheduled(date, type, id, true, false);
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('No, just this one'),
                  onPressed: () {
                    deleteScheduled(date, type, id, false, false);
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      deleteRow(date, type, id);
    }
  }

  void deleteScheduled(
      String date, String type, String id, bool all, bool stop) async {
    var event = widget.db.child(date).child(type).child(id);
    var eventParsed = await event.once();
    int sid = eventParsed.value["sid"];
    String sidStr = sid.toString();

    if (all) {
      var instances =
          widget.db.child("schedule").child(sidStr).child("instances").once();
      var instancesGotten = await instances;

      //print(instancesGotten.value);
      instancesGotten.value
          .forEach((k, v) => {deleteRow(k, type, v.toString())});

      widget.db.child("schedule").child(sidStr).child("instances").set(null);
      widget.db.child("schedule").child(sidStr).child("stop").set(true);
    } else {
      widget.db
          .child("schedule")
          .child(sidStr)
          .child("instances")
          .child(date)
          .set(null);
      deleteRow(date, type, id);
    }
  }

  void deleteRow(String date, String type, String id) async {
    widget.db.child(date).child(type).child(id).set(null);
  }
}
