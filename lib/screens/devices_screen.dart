import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:greencap/screens/screens.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'dart:convert';

class DevicesScreen extends StatefulWidget {
  @override
  _DevicesScreenState createState() => _DevicesScreenState();
}

class _DevicesScreenState extends State<DevicesScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;
  String _category = "All";
  double _width;
  bool resultz = false;
  String email = "";
  String password = "";
  String token = "";
  List<Device> deviceList = [];

  _DevicesScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));

    await db.once().then((value) => {ds = value});

    db.onValue.listen((event) {
      setState(() {
        db = db;
        ds = event.snapshot;
      });
    });
    setState(() {});
  }

  getDevices(DatabaseReference db) async {
    DatabaseReference dbRef = db.child("deviceAccount");
    DataSnapshot nullChecker = await dbRef.once();
    if (nullChecker.value == null) {
      print("No Devices");
      resultz = false;
    } else {
      print('Data : ${nullChecker.value.toString()}');
      email = nullChecker.value["Email"].toString();
      password = nullChecker.value["Password"].toString();
      String limitString = nullChecker.value.length.toString();
      int limitInt = int.parse(limitString) - 3;
      print(limitInt);
      for (var i = 0; i <= limitInt; i++) {
        String devId = nullChecker.value["$i"]["DeviceId"].toString();
        String devModel = nullChecker.value["$i"]["DeviceModel"].toString();
        String devName = nullChecker.value["$i"]["DeviceName"].toString();
        // print(devId);
        // print(devModel);
        // print(devName);
        Device regDevice = Device(devId, devName, devModel);
        if (deviceList.length == 0) {
          deviceList.add(regDevice);
        } else if (deviceList.length < limitInt) {
          deviceList.add(regDevice);
        }
      }
      resultz = true;
      print("oka");
    }
  }

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    if (db == null) {
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    } else {
      getDevices(db);
      print(resultz);
      if (resultz) {
        return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
              child: Container(
                  margin: EdgeInsets.only(top: 10, left: _width * 0.05 * 0.5),
                  width: _width * 0.95,
                  child: Column(children: [
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        width: _width * 0.95,
                        child: Text("Your Devices",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 28,
                                color: Color(0xFF053A54)))),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      width: _width * 0.95,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddDevicesScreen()));
                        },
                        child: const Text('Add New Smart Device',
                            style: TextStyle(fontSize: 20)),
                      ),
                    ),
                    for (var device in deviceList)
                      ListTile(
                        title: Text(device.deviceName),
                        subtitle: Text(device.deviceModel),
                        leading: Icon(Icons.devices),
                      ),
                  ]))),
        );
      } else {
        return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
              child: Container(
                  margin: EdgeInsets.only(top: 10, left: _width * 0.05 * 0.5),
                  width: _width * 0.95,
                  child: Column(children: [
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        width: _width * 0.95,
                        child: Text("Your Devices",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 28,
                                color: Color(0xFF053A54)))),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      width: _width * 0.95,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddDevicesScreen()));
                        },
                        child: const Text('Add New Smart Device',
                            style: TextStyle(fontSize: 20)),
                      ),
                    ),
                    Text("You have no devices"),
                  ]))),
        );
      }
    }
  }

  Column loadResults() {
    var device = TextEditingController();
    var duration = TextEditingController();
    var frequency = TextEditingController();
    var type = TextEditingController();
    List<Widget> results = new List();
    Map<dynamic, dynamic> schedule = ds.value["schedule"];

    if (schedule != null) {
      schedule.forEach((key, value) {
        if (((key != "id" && value["type"] == _category) ||
                (key != "id" && _category == "All")) &&
            value["stop"] == false) {
          results.add(Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
              color: Color(0xFFE4ECEF),
              width: _width * 0.95,
              child: Row(children: [
                Expanded(
                    flex: 9,
                    child: Column(children: [
                      Row(children: [
                        Text(
                          value["device"].toString(),
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.w600,
                              fontSize: 24,
                              color: Color(0xFF187DC9)),
                        )
                      ]),
                      Row(children: [
                        Text("Duration: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["duration"].toString() + " minutes",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Type: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["type"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Frequency: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["freq"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Start Date: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["startDate"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Last Logged: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["lastLogged"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                    ])),
                Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      IconButton(
                          icon: Icon(Icons.edit),
                          color: Color(0xFF05a869),
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 40),
                          onPressed: () {
                            device.text = value["device"].toString();
                            duration.text = value["duration"].toString();
                            frequency.text = value["freq"].toString();
                            type.text = value["type"].toString();
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text('Update Scheduled Event'),
                                content: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      new TextField(
                                        decoration: new InputDecoration(
                                            helperText: "Device"),
                                        controller: device,
                                      ),
                                      new TextField(
                                        decoration: new InputDecoration(
                                            helperText: "Duration"),
                                        controller: duration,
                                      ),
                                      new TextField(
                                        decoration: new InputDecoration(
                                            helperText: "Frequency"),
                                        controller: frequency,
                                      ),
                                      new TextField(
                                        decoration: new InputDecoration(
                                            helperText: "Type"),
                                        controller: type,
                                      ),
                                    ],
                                  ),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Cancel'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text('Update'),
                                    onPressed: () {
                                      editScheduled(
                                          key,
                                          device.text,
                                          duration.text,
                                          frequency.text,
                                          type.text);
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              ),
                            );
                          }),
                      IconButton(
                          icon: Icon(Icons.delete),
                          color: Color(0xFFb31018),
                          padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text('Delete Scheduled Event?'),
                                content: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      Text(
                                          "Are you sure you want to delete this scheduled event?"),
                                    ],
                                  ),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Cancel'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text('Delete'),
                                    onPressed: () {
                                      deleteScheduled(key);
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              ),
                            );
                          })
                    ],
                  ),
                )
              ])));
        }
      });
    }
    return Column(children: results);
  }

  void deleteScheduled(String key) async {
    db.child("schedule").child(key).update({"stop": true});
  }

  void editScheduled(String key, String device, String duration,
      String frequency, String type) {
    db.child("schedule").child(key).update({"device": device});
    db.child("schedule").child(key).update({"duration": duration});
    db.child("schedule").child(key).update({"freq": frequency});
    db.child("schedule").child(key).update({"type": type});
  }
}

_LoginStatus(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the status code
  int statusCode = response.statusCode;
  return statusCode;
}

_LoginErrorCode(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  int errorCode = map['error_code'];
  return errorCode;
}

_LoginErrorMessage(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  String errorMessage = map['msg'];
  return errorMessage;
}

_LoginToken(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  String token = map['result']["token"];
  return token;
}

// Probably should have a method going here that checks the user has atleast one device. Yolo.

_GetDeviceList(String token) async {
  // set up POST request arguments
  String url = "https://wap.tplinkcloud.com?token=$token";
  Map<String, String> headers = {"Content-type": "application/json"};
  String json = '{"method":"getDeviceList"}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  int numDevices = map['result']['deviceList'].length;
  List<Device> deviceList = [];
  for (var i = 0; i < numDevices; i++) {
    String deviceId = map['result']['deviceList'][i]['deviceId'];
    String deviceName = map['result']['deviceList'][i]['deviceName'];
    String deviceModel = map['result']['deviceList'][i]['deviceModel'];
    deviceList.add(Device(deviceId, deviceName, deviceModel));
    //print(deviceList[i].deviceId);
  }
  return deviceList;
}
