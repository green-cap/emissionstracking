import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ScheduledScreen extends StatefulWidget {
  @override
  _ScheduledScreenState createState() => _ScheduledScreenState();
}

class _ScheduledScreenState extends State<ScheduledScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;
  String _category = "All";
  double _width;

  _ScheduledScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));

    await db.once().then((value) => {ds = value});

    db.onValue.listen((event) {
      if (mounted) {
        setState(() {
          db = db;
          ds = event.snapshot;
        });
      }
    });

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    if (db == null) {
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    } else {
      return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Color(0xFF053a54),
          title: Image.asset(
            'lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.only(top: 10, left: _width * 0.05 * 0.5),
                width: _width * 0.95,
                child: Column(children: [
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      width: _width * 0.95,
                      child: Text("Your Scheduled Emissions",
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.w600,
                              fontSize: 28,
                              color: Color(0xFF053A54)))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: DropdownSearch<String>(
                          mode: Mode.MENU,
                          items: ["All", "Electricity", "Transport", "Food"],
                          label: "Emissions Category",
                          showSelectedItem: true,
                          hint: "Filter by emissions category",
                          maxHeight: 240,
                          selectedItem: _category,
                          onChanged: (String selectedItem) => setState(() {
                                _category = selectedItem;
                              }))),
                  loadResults()
                ]))),
      );
    }
  }

  Column loadResults() {
    var duration = TextEditingController();
    List<Widget> results = new List();
    Map<dynamic, dynamic> schedule = ds.value["schedule"];

    if (schedule != null) {
      schedule.forEach((key, value) {
        if (((key != "id" && value["type"] == _category) ||
                (key != "id" && _category == "All")) &&
            value["stop"] == false) {
          results.add(Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
              color: Color(0xFFE4ECEF),
              width: _width * 0.95,
              child: Row(children: [
                Expanded(
                    flex: 9,
                    child: Column(children: [
                      Row(children: [
                        Text(
                          value["device"].toString(),
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.w600,
                              fontSize: 24,
                              color: Color(0xFF187DC9)),
                        )
                      ]),
                      Row(children: [
                        Text("Duration: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["duration"].toString() + " minutes",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Type: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["type"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Frequency: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["freq"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Start Date: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["startDate"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                      Row(children: [
                        Text("Last Logged: ",
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF0390F9))),
                        Text(value["lastLogged"].toString(),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Color(0xFF000000)))
                      ]),
                    ])),
                Expanded(
                    flex: 1,
                    child: Column(children: [
                      IconButton(
                          icon: Icon(Icons.edit),
                          color: Color(0xFF05a869),
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 40),
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  duration.text = value["duration"].toString();
                                  String frequencyValue =
                                      value["freq"].toString();
                                  String typeValue = value["type"].toString();
                                  String deviceValue =
                                      value["device"].toString();
                                  return StatefulBuilder(
                                      builder: (context, setState) {
                                    return AlertDialog(
                                        title: Text('Update ' +
                                            value["device"].toString() +
                                            " Event"),
                                        content: SingleChildScrollView(
                                          child: ListBody(
                                            children: <Widget>[
                                              new TextField(
                                                decoration: new InputDecoration(
                                                    labelText:
                                                        "Duration (mins)"),
                                                controller: duration,
                                              ),
                                              Row(children: [
                                                Text("Frequency (Days):  "),
                                                DropdownButton<String>(
                                                  value: frequencyValue,
                                                  icon: Icon(
                                                      Icons.arrow_downward),
                                                  iconSize: 24,
                                                  elevation: 16,
                                                  style: TextStyle(
                                                      color: Colors.deepPurple),
                                                  underline: Container(
                                                    height: 2,
                                                    color:
                                                        Colors.deepPurpleAccent,
                                                  ),
                                                  items: <String>[
                                                    '1',
                                                    '7',
                                                    '14'
                                                  ].map<
                                                          DropdownMenuItem<
                                                              String>>(
                                                      (String value) {
                                                    return DropdownMenuItem<
                                                        String>(
                                                      value: value,
                                                      child: Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (String newValue) {
                                                    setState(() {
                                                      frequencyValue = newValue;
                                                    });
                                                  },
                                                )
                                              ]),
                                            ],
                                          ),
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('Cancel'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          FlatButton(
                                              child: Text('Update'),
                                              onPressed: () {
                                                editScheduled(
                                                    key,
                                                    deviceValue,
                                                    duration.text,
                                                    frequencyValue,
                                                    typeValue);
                                                Navigator.of(context).pop();
                                              })
                                        ]);
                                  });
                                });
                          }),
                      IconButton(
                          icon: Icon(Icons.delete),
                          color: Color(0xFFb31018),
                          padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text('Delete Scheduled Event?'),
                                content: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      Text(
                                          "Are you sure you want to delete this scheduled event?"),
                                    ],
                                  ),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Cancel'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text('Delete'),
                                    onPressed: () {
                                      deleteScheduled(key);
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              ),
                            );
                          })
                    ]))
              ])));
        }
      });
    }
    return Column(children: results);
  }

  void deleteScheduled(String key) async {
    db.child("schedule").child(key).update({"stop": true});
  }

  void editScheduled(String key, String device, String duration,
      String frequency, String type) {
    db.child("schedule").child(key).update({"device": device});
    db.child("schedule").child(key).update({"duration": int.parse(duration)});
    db.child("schedule").child(key).update({"freq": int.parse(frequency)});
    db.child("schedule").child(key).update({"type": type});
  }
}
