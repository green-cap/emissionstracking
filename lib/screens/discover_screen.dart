import 'package:flutter/material.dart';
import 'package:webfeed/webfeed.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'dart:core';
import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';

class DiscoverScreen extends StatefulWidget {

  DiscoverScreen() : super();

  final String title = "Discovery Page";

  @override
  DiscoveryScreenState createState() => DiscoveryScreenState();


  
}
  class DiscoveryScreenState extends State<DiscoverScreen>{
    static const String FEED_URL = 'https://www.sciencedaily.com/rss/top/environment.xml';
    RssFeed _feed;
    String _title;
    static const String  loadingFeedMsg = 'Loading Feed...';
    static const String  feedLoadErrorMsg = 'Error Loading Feed';

    static const String placeholderImg = 'lib/assets/placeholder.png';

    static const String feedOpenErrorMsg = 'Error Opening Feed.';

    dynamic listImages = ['lib/assets/green.jpg','lib/assets/pollution.jpg', 'lib/assets/world.jpg', 'lib/assets/forest.jpg'];
  
    Random rnd;
    
    GlobalKey<RefreshIndicatorState> _refreshKey;

    updateTitle(title){
      setState((){
        _title = title;
      });
    }

    updateFeed(feed){
      setState((){
        _feed = feed;
      });
    }



     Future<void> openFeed(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: false,
      );
      return;
    }
    updateTitle(feedOpenErrorMsg);
  }

    load() async{
      updateTitle(loadingFeedMsg);
      loadFeed().then((result){
        if(null == result || result.toString().isEmpty){
            updateTitle(feedLoadErrorMsg);
            return;
        }
        updateFeed(result);
        updateTitle(_feed.title);
      });
    }

    Future<RssFeed> loadFeed() async{
       try {
        final client = http.Client();
        final response = await client.get(FEED_URL);
        return RssFeed.parse(response.body);
      } catch (e) {
     //
      }
      return null;
    }
    

     Image randomImg() {
      int max = listImages.length - 1;
      rnd = new Random();
      int r = rnd.nextInt(max + 1);
      String imageName  = listImages[r].toString();
      return Image.asset(imageName.toString());
    }

    @override void initState(){
      super.initState();
      updateTitle(widget.title);
      load();
    }

    title(title) {
    return Text(
      title,
      style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }

   subtitle(subTitle) {
    return Text(
      subTitle,
      style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w100),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
    );
  }

  thumbnail(imageUrl) {
    return Padding(
      padding: EdgeInsets.only(left: 15.0),
      child: CachedNetworkImage(
        placeholder: (context, url) => Image.asset(placeholderImg),
        imageUrl: imageUrl,
        height: 50,
        width: 70,
        alignment: Alignment.center,
        fit: BoxFit.fill,
      ),
    );
  }

   rightIcon() {
    return Icon(
      Icons.keyboard_arrow_right,
      color: Colors.grey,
      size: 30.0,
    );
  }

   list() {
    
    return ListView.builder(


      
      itemCount: _feed.items.length,
      itemBuilder: (BuildContext context, int index) {
        final item = _feed.items[index];
        return new Card(
         color: Colors.white70,
         child: Column(
         children:<Widget>[
                       
         ListTile(
           
          leading: CircleAvatar(radius: 20.0, backgroundImage: AssetImage('lib/assets/profile_picture.png'),),
          title: title(item.title),
         
          subtitle: Row(
            children:<Widget>[
              
              subtitle(item.pubDate),
                      Icon (Icons.timelapse, size: 16.0, )
                    ]

                  ), 

          trailing: rightIcon(),
          contentPadding: EdgeInsets.all(5.0),
          onTap: () => openFeed(item.link),
        ),
        Container(
                  child: Text(item.description, style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "Avenir")), 
                  width: MediaQuery.of(context).size.width,
              ),              
        Container(
         

                child: randomImg(),
                height: 250,
                width: MediaQuery.of(context).size.width

              ),
              
              ])
        );

        

        
      },
    );
  }

  

   isFeedEmpty() {
    return null == _feed || null == _feed.items;
  }

   body() {
     
    return isFeedEmpty()
        ? Center(
            child: CircularProgressIndicator(),
          )
        : RefreshIndicator(
            key: _refreshKey,
            child: list(),
            onRefresh: () => load(),
          );
  }
 
  @override
  Widget build(BuildContext context) {

   
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Color(0xFF053a54),
        
        
        title: Image.asset('lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,),
            centerTitle: true

      ),
   

      body: body(),
      
    );
  }
}


    

  




