import 'package:flutter/material.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:greencap/screens/screens.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:dropdown_search/dropdown_search.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  FirebaseAuth userauth = FirebaseAuth.instance;
  DatabaseReference db;
  DataSnapshot ds;
  DataSnapshot dsUnits;
  _ProfileScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance
        .reference()
        .child(prefs.getString("uid"))
        .child("user");

    await db.once().then((value) => {ds = value});
    await FirebaseDatabase.instance
        .reference()
        .child("units")
        .once()
        .then((value) => {dsUnits = value});

    db.onValue.listen((event) {
      if (mounted) {
        setState(() {
          db = db;
          ds = event.snapshot;
        });
      }
    });

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    var first = TextEditingController();
    var last = TextEditingController();
    var email = TextEditingController();
    final TextStyle tableRowStyle = TextStyle(
        fontFamily: "Avenir",
        fontWeight: FontWeight.w600,
        fontSize: 17,
        color: Color(0xFF0390F9));

    if (db == null) {
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    } else {
      return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Color(0xFF053a54),
          title: Image.asset(
            'lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Column(children: [
          Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              width: width * 0.95,
              child: Text("Settings",
                  style: TextStyle(
                      fontFamily: "Avenir",
                      fontWeight: FontWeight.w600,
                      fontSize: 30,
                      color: Color(0xFF053A54)))),
          Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
              color: Color(0xFFE4ECEF),
              width: width * 0.95,
              child: Row(children: [
                Expanded(
                    flex: 9,
                    child: Column(
                      children: [
                        Row(children: [
                          Text("Your Details",
                              style: TextStyle(
                                  fontFamily: "Avenir",
                                  fontWeight: FontWeight.w600,
                                  fontSize: 24,
                                  color: Color(0xFF187DC9)))
                        ]),
                        Row(children: [
                          Text("First Name: " + ds.value["first"],
                              style: tableRowStyle)
                        ]),
                        Row(children: [
                          Text("Last Name: " + ds.value["last"],
                              style: tableRowStyle)
                        ]),
                        Row(children: [
                          Text("Email: " + ds.value["email"],
                              style: tableRowStyle)
                        ]),
                        Row(children: [
                          Text("Display Units: " + ds.value["units"],
                              style: tableRowStyle)
                        ]),
                      ],
                    )),
                Expanded(
                  flex: 1,
                  child: IconButton(
                      icon: Icon(Icons.edit),
                      color: Color(0xFF05a869),
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              first.text = ds.value["first"];
                              last.text = ds.value["last"];
                              email.text = ds.value["email"];
                              String unitValue = ds.value["units"];
                              return StatefulBuilder(
                                  builder: (context, setState) {
                                return AlertDialog(
                                  title: Text('Update User Profile'),
                                  content: SingleChildScrollView(
                                    child: ListBody(
                                      children: <Widget>[
                                        new TextField(
                                          decoration: new InputDecoration(
                                              labelText: "First Name"),
                                          controller: first,
                                        ),
                                        new TextField(
                                          decoration: new InputDecoration(
                                              labelText: "Last Name"),
                                          controller: last,
                                        ),
                                        new TextField(
                                          decoration: new InputDecoration(
                                              labelText: "Email"),
                                          controller: email,
                                        ),
                                        Row(children: [
                                          Text("Units:  "),
                                          DropdownButton<String>(
                                            value: unitValue,
                                            icon: Icon(Icons.arrow_downward),
                                            iconSize: 24,
                                            elevation: 16,
                                            style: TextStyle(
                                                color: Colors.deepPurple),
                                            underline: Container(
                                              height: 2,
                                              color: Colors.deepPurpleAccent,
                                            ),
                                            items: loadUnits(),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                unitValue = newValue;
                                              });
                                            },
                                          )
                                        ]),
                                      ],
                                    ),
                                  ),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text('Cancel'),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    FlatButton(
                                      child: Text('Update'),
                                      onPressed: () {
                                        editUser(first.text, last.text,
                                            email.text, unitValue);
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              });
                            });
                      }),
                )
              ])),
          Container(
              margin: EdgeInsets.only(top: 20),
              child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DevicesScreen()));
                  },
                  child: Row(children: [
                    Icon(Icons.devices),
                    Text(
                      " Smart Devices",
                    ),
                  ]))),
          Container(
              margin: EdgeInsets.only(top: 5),
              child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ScheduledScreen()));
                  },
                  child: Row(children: [
                    Icon(Icons.history),
                    Text(
                      "View Scheduled Emissions",
                    ),
                  ]))),
          Container(
              margin: EdgeInsets.only(top: 5),
              child: FlatButton(
                  onPressed: () async {
                    await userauth.signOut();
                    await _prefs
                      ..clear();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginScreen()));
                  },
                  child: Row(children: [
                    Icon(Icons.exit_to_app),
                    Text(
                      "Logout",
                    ),
                  ])))
        ])),
      );
    }
  }

  void editUser(String first, String last, String email, String units) {
    db.update({"first": first});
    db.update({"last": last});
    db.update({"email": email});
    db.update({"units": units});
  }

  List<DropdownMenuItem<String>> loadUnits() {
    List<DropdownMenuItem<String>> units = List();
    print(dsUnits.value.keys);
    for (String key in dsUnits.value.keys) {
      units.add(DropdownMenuItem<String>(
        value: key,
        child: Text(key),
      ));
    }
    return units;
  }
}
