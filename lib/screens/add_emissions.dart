import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'package:greencap/scheduler/scheduler.dart';
import 'package:greencap/widgets/add_emissions_bubble.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/widgets.dart';
import 'package:greencap/models/models.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:toast/toast.dart';
import 'adddevices_screen.dart';

class AddEmissions extends StatefulWidget {
  @override
  _AddEmissionsState createState() => _AddEmissionsState();
}

class _AddEmissionsState extends State<AddEmissions> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;
  DataSnapshot devices;
  _AddEmissionsState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));

    await db.once().then((value) => {ds = value});
    await FirebaseDatabase.instance
        .reference()
        .child("devices")
        .once()
        .then((value) => {devices = value});

    db.onValue.listen((event) {
      if (mounted) {
        setState(() {
          db = db;
          ds = event.snapshot;
        });
      }
    });

    if (mounted) {
      setState(() {});
    }
  }

  DateTime _date = new DateTime.now();
  final TextStyle title = TextStyle(
    fontFamily: "Avenir",
    fontWeight: FontWeight.w500,
    fontSize: 25,
    decoration: TextDecoration.underline,
    color: Colors.white,
  );

  final TextStyle tableHeadStyle = TextStyle(
    fontFamily: "Avenir",
    fontWeight: FontWeight.w500,
    fontSize: 15,
    decoration: TextDecoration.underline,
    color: Colors.white,
  );

  final TextStyle tableRowStyle = TextStyle(
    fontFamily: "Avenir",
    fontWeight: FontWeight.w500,
    fontSize: 15,
    color: Colors.white,
  );

  String email = "";
  String password = "";
  String token = "";

  @override
  Widget build(BuildContext context) {
    if (db == null) {
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    }

    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Color(0xFF053a54),
        title: Image.asset(
          'lib/assets/greencap.png',
          width: 80,
          fit: BoxFit.fitWidth,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(
              horizontal: 10.0,
              vertical: 10.0,
            ),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  //Date and Arrows Row
                  Row(
                    children: [
                      Expanded(
                          child: IconButton(
                        icon: Icon(
                          Icons.arrow_left,
                          size: 50,
                          color: Color(0xFF053a54),
                        ),
                        onPressed: () {
                          setState(() {
                            _date = _date.subtract(new Duration(days: 1));
                            //Get Dates
                          });
                        },
                      )),
                      Column(
                        children: [
                          Padding(
                              child: Text(
                                getFormattedDate(),
                                style: TextStyle(
                                    fontFamily: 'Avenir',
                                    fontSize: 20,
                                    color: Color(0xFF053a54),
                                    fontWeight: FontWeight.w600),
                              ),
                              padding: EdgeInsets.fromLTRB(0, 20, 0, 0)),
                          Padding(
                              child: RaisedButton(
                                  color: Color(0xFF05CC7F),
                                  elevation: 5.0,
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          DeviceK device;
                                          TextEditingController duration =
                                              TextEditingController();

                                          String repValue = '1';
                                          String eventType = 'Electricity';
                                          bool startToday = false;

                                          return StatefulBuilder(
                                              builder: (context, setState) {
                                            return Dialog(
                                                child: Container(
                                                    height: 500,
                                                    padding: EdgeInsets.all(20),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                            "Add Scheduled Event"),
                                                        Row(
                                                          children: [
                                                            Text('Type:  '),
                                                            DropdownButton<
                                                                String>(
                                                              value: eventType,
                                                              icon: Icon(Icons
                                                                  .arrow_downward),
                                                              iconSize: 24,
                                                              elevation: 16,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .deepPurple),
                                                              underline:
                                                                  Container(
                                                                height: 2,
                                                                color: Colors
                                                                    .deepPurpleAccent,
                                                              ),
                                                              onChanged: (String
                                                                  newValue) {
                                                                setState(() {
                                                                  eventType =
                                                                      newValue;
                                                                });
                                                              },
                                                              items: <String>[
                                                                'Electricity',
                                                                'Transport',
                                                                'Food'
                                                              ].map<
                                                                  DropdownMenuItem<
                                                                      String>>((String
                                                                  value) {
                                                                return DropdownMenuItem<
                                                                    String>(
                                                                  value: value,
                                                                  child: Text(
                                                                      value),
                                                                );
                                                              }).toList(),
                                                            )
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                                child:
                                                                    DropdownSearch<
                                                                        DeviceK>(
                                                              mode: Mode
                                                                  .BOTTOM_SHEET,
                                                              showSelectedItem:
                                                                  false,
                                                              showSearchBox:
                                                                  true,
                                                              itemAsString:
                                                                  (DeviceK d) =>
                                                                      d.name,
                                                              items:
                                                                  generateSearchItems(
                                                                      eventType),
                                                              onChanged: (DeviceK
                                                                      data) =>
                                                                  device = data,
                                                              label: eventType ==
                                                                      'Food'
                                                                  ? 'Food'
                                                                  : 'Device',
                                                            )),
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                              child:
                                                                  new TextField(
                                                                autofocus: true,
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                controller:
                                                                    duration,
                                                                decoration: new InputDecoration(
                                                                    labelText: eventType ==
                                                                            'Food'
                                                                        ? 'Amount(gms)'
                                                                        : 'Duration(mins)',
                                                                    hintText:
                                                                        'e.g. 5'),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                                'Repeat every? (days):  '),
                                                            DropdownButton<
                                                                String>(
                                                              value: repValue,
                                                              icon: Icon(Icons
                                                                  .arrow_downward),
                                                              iconSize: 24,
                                                              elevation: 16,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .deepPurple),
                                                              underline:
                                                                  Container(
                                                                height: 2,
                                                                color: Colors
                                                                    .deepPurpleAccent,
                                                              ),
                                                              onChanged: (String
                                                                  newValue) {
                                                                setState(() {
                                                                  repValue =
                                                                      newValue;
                                                                });
                                                              },
                                                              items: <String>[
                                                                '1',
                                                                '7',
                                                                '14'
                                                              ].map<
                                                                  DropdownMenuItem<
                                                                      String>>((String
                                                                  value) {
                                                                return DropdownMenuItem<
                                                                    String>(
                                                                  value: value,
                                                                  child: Text(
                                                                      value),
                                                                );
                                                              }).toList(),
                                                            ),
                                                            Expanded(
                                                              child: Text("Next: " +
                                                                  GreenCapScheduler.formatAnyDate(
                                                                      _date.add(
                                                                          new Duration(
                                                                              days: int.parse(repValue))))),
                                                            )
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                                "Add to today?"),
                                                            Checkbox(
                                                                value:
                                                                    startToday,
                                                                onChanged: (e) {
                                                                  setState(() {
                                                                    startToday =
                                                                        e;
                                                                  });
                                                                })
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            RaisedButton(
                                                              child: Text(
                                                                  "Cancel"),
                                                              onPressed: () =>
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop(),
                                                            ),
                                                            Spacer(),
                                                            RaisedButton(
                                                              child:
                                                                  Text("Add"),
                                                              onPressed: () {
                                                                if (duration
                                                                        .text ==
                                                                    "") {
                                                                  return;
                                                                }

                                                                int zeroCheck =
                                                                    int.parse(
                                                                        duration
                                                                            .text);
                                                                if (zeroCheck ==
                                                                        0 ||
                                                                    zeroCheck <
                                                                        0) {
                                                                  return;
                                                                }

                                                                addRepetitionRow(
                                                                    device.name,
                                                                    duration
                                                                        .text,
                                                                    eventType,
                                                                    repValue,
                                                                    getFormattedDate(),
                                                                    GreenCapScheduler.formatAnyDate(_date.add(
                                                                        new Duration(
                                                                            days:
                                                                                int.parse(repValue)))),
                                                                    startToday,
                                                                    device.id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    )));
                                          });
                                        });
                                  },
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      side: BorderSide(color: Colors.green)),
                                  // icon: Icon(Icons.add_alarm),
                                  child: Text("ADD SCHEDULED EVENT",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Avenir",
                                          fontWeight: FontWeight.w900,
                                          fontSize: 15))),
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 10)),
                        ],
                      ),
                      Expanded(
                          child: IconButton(
                        icon: Icon(
                          Icons.arrow_right,
                          color: Color(0xFF053a54),
                          size: 50,
                        ),
                        onPressed: () {
                          setState(() {
                            _date = _date.add(new Duration(days: 1));
                            //Push Data
                          });
                        },
                      )),
                    ],
                  ),

                  AddEmissionsBubble(
                      "Electricity",
                      Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                      Color(0xFF086096),
                      getFormattedDate(),
                      ds,
                      "Lightbulb",
                      db,
                      false,
                      devices),
                  SizedBox(
                    height: 10,
                  ),
                  AddEmissionsBubble(
                      "Transport",
                      Icon(
                        Icons.directions_car,
                        color: Colors.white,
                      ),
                      Color(0xFF229ae3),
                      getFormattedDate(),
                      ds,
                      "Car",
                      db,
                      false,
                      devices),
                  SizedBox(
                    height: 10,
                  ),
                  AddEmissionsBubble(
                      "Food",
                      Icon(
                        Icons.fastfood,
                        color: Colors.white,
                      ),
                      Color(0xFF5fbffa),
                      getFormattedDate(),
                      ds,
                      "Steak",
                      db,
                      true,
                      devices),
                  RaisedButton(
                    child: Text("Sync Smart Devices"),
                    onPressed: () {
                      _syncSmartDeviceData(2020, db);
                    },
                  )
                ])),
      ),
    );
  }

  // Smart Device Code:
  _getEmail(DatabaseReference db) async {
    DatabaseReference dbRef = db.child("deviceAccount");
    DataSnapshot nullChecker = await dbRef.once();
    if (nullChecker.value == null) {
      print("No Devices");
    } else {
      print('Data : ${nullChecker.value.toString()}');
      String email = nullChecker.value["Email"].toString();
      return email;
    }
  }

  // Smart Device Code:
  _getPassword(DatabaseReference db) async {
    DatabaseReference dbRef = db.child("deviceAccount");
    DataSnapshot nullChecker = await dbRef.once();
    if (nullChecker.value == null) {
      print("No Devices");
    } else {
      print('Data : ${nullChecker.value.toString()}');
      String password = nullChecker.value["Password"].toString();
      return password;
    }
  }

  _getLoginToken(String cloudUserName, String cloudPassword) async {
    // set up POST request arguments
    String url = 'https://wap.tplinkcloud.com';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
            cloudUserName +
            '","cloudPassword": "' +
            cloudPassword +
            '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // Stores the response
    String body = response.body;
    Map<String, dynamic> map = jsonDecode(body);
    print(map);
    String token = map['result']["token"];
    print(token);
    return token.toString();
  }

  _getDeviceList(String token) async {
    // set up POST request arguments
    String url = "https://wap.tplinkcloud.com?token=$token";
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"method":"getDeviceList"}';
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // Stores the response
    String body = response.body;
    Map<String, dynamic> map = jsonDecode(body);
    int numDevices = map['result']['deviceList'].length;
    List<Device> deviceList = [];
    for (var i = 0; i < numDevices; i++) {
      String deviceId = map['result']['deviceList'][i]['deviceId'];
      String deviceName = map['result']['deviceList'][i]['deviceName'];
      String deviceModel = map['result']['deviceList'][i]['deviceModel'];
      deviceList.add(Device(deviceId, deviceName, deviceModel));
      //print(deviceList[i].deviceId);
    }
    return deviceList;
  }

  _getEnergyUsageDaily(String token, String deviceId, int year) async {
    // set up POST request arguments
    List<dynamic> numEntries = [];
    List<Entry> allEntries = [];

    for (var i = 1; i < 13; i++) {
      int month = i;
      print("Month Value:" + month.toString());

      String url = "https://wap.tplinkcloud.com?token=$token";
      Map<String, String> headers = {"Content-type": "application/json"};
      String json =
          '{"method": "passthrough","params": {"deviceId": "$deviceId","requestData": {"context": {"child_ids": ["CHILD_ID"]},"emeter": {"get_daystat": {"month": $month,"year": $year}}}}}';
      // make POST request
      Response response = await post(url, headers: headers, body: json);
      // Stores the response
      String body = response.body;
      print("Body Value:" + body);
      Map<String, dynamic> map = jsonDecode(body);
      List<dynamic> numEntriesMap =
          map['result']['responseData']['emeter']['get_daystat']['day_list'];
      print("numEntriesMap Value: " +
          map['result']['responseData']['emeter']['get_daystat']['day_list']
              .toString());
      if (numEntriesMap.isNotEmpty) {
        numEntries.addAll(numEntriesMap);
        int allEntriesLength = numEntries.length;
        print("num days: " + allEntriesLength.toString());
        for (var i = 0; i < allEntriesLength; i++) {
          int month = numEntries[i]['month'];
          int year = numEntries[i]['year'];
          int energy = numEntries[i]['energy_wh'];
          int day = numEntries[i]['day'];
          print(i.toString() +
              " : " +
              day.toString() +
              " : " +
              month.toString() +
              " : " +
              year.toString() +
              " : " +
              energy.toString());
          Entry entry = Entry(month, year, energy, day);
          allEntries.add(entry);
        }
      }
    }
    print("333333333333333333333333333333333333333333333333");
    allEntries.forEach((element) {
      print(element.day.toString() + " : " + element.month.toString());
    });
    print(allEntries.length);
    return allEntries;
  }

  _syncSmartDeviceData(int year, DatabaseReference db) async {
    // Get from database
    String email = await _getEmail(db);
    print("Email is: " + email);
    String password = await _getPassword(db);
    print("Password is: " + password);
    String token = await _getLoginToken(email, password);
    print("Token is: " + token);
    List<Device> deviceList = await _getDeviceList(token);
    for (var i = 0; i < deviceList.length; i++) {
      String deviceName = deviceList[i].deviceName;
      String deviceModel = deviceList[i].deviceModel;
      String deviceId = deviceList[i].deviceId;
      print("Device $i: " + deviceName + ", " + deviceModel + ", " + deviceId);
      List<Entry> entries = await _getEnergyUsageDaily(token, deviceId, year);

      DatabaseReference dbRef = db.child("deviceAccount");
      DataSnapshot nullChecker = await dbRef.once();
      if (nullChecker.value != null) {
        DatabaseReference dbRef = db.child("deviceAccount");
        for (var i = 0; i < entries.length; i++) {
          String date = entries[i].day.toString() +
              "-" +
              entries[i].month.toString() +
              "-" +
              entries[i].year.toString();
          print(date);
          DatabaseReference stitch = db.child("deviceAccount");
          DataSnapshot nullCheckerTwo = await stitch.once();
          // print(nullCheckerTwo.value['$date']);
          if (nullCheckerTwo.value['$date'] == null) {
            print("null");
            dbRef.child("$date").set({'Energy': entries[i].energy});
          } else {
            print("not null");
            int dbEnergy = nullCheckerTwo.value['$date']['Energy'];
            print(dbEnergy);
            int apiEnergy = entries[i].energy;
            print(apiEnergy.toString());
            if (dbEnergy < apiEnergy) {
              dbRef.child("$date").update({'Energy': apiEnergy});
            }
          }
        }
      }
    }

    // Get Token

    // Get Monthly Data

    // Check if already exists
    // If doesnt exist -> add all
    // If does exist -> update all
  }

  //BE VERY CAREFUL CHANGING THIS
  //USED AS KEY IN THE DATABASE
  String getFormattedDate() {
    return GreenCapScheduler.formatAnyDate(_date);
  }

  List<DeviceK> generateSearchItems(String type) {
    var a = devices.value[type];
    List<DeviceK> items = [];
    for (var key in a.keys) {
      if (key == 'id') {
        continue;
      }

      items.add(DeviceK(id: int.parse(key), name: a[key]['name']));
    }
    return items;
  }

  void addRepetitionRow(
      String device,
      String duration,
      String type,
      String freq,
      String startDate,
      String nextDate,
      bool addToToday,
      int deviceId) async {
    DatabaseReference dbRef = db.child("schedule");
    DatabaseReference dbRef2 = db.child(startDate).child(type);

    DataSnapshot nullChecker = await dbRef.once();
    DataSnapshot nullChecker2 = await dbRef2.once();
    int id = 0;
    int id2 = 0;

    if (nullChecker.value == null) {
      Map mp = {
        "device": device,
        "deviceId": deviceId,
        "duration": int.parse(duration),
        "type": type,
        "freq": int.parse(freq),
        "startDate": startDate,
        "lastLogged": startDate,
        "stop": false,
      };
      dbRef.child("0").set(mp);
      dbRef.update({"id": 1});
    } else {
      Map mp = {
        "device": device,
        "deviceId": deviceId,
        "duration": int.parse(duration), //Add duration check
        "type": type,
        "freq": int.parse(freq),
        "startDate": startDate,
        "lastLogged": startDate,
        "stop": false,
      };
      dbRef.child(nullChecker.value["id"].toString()).set(mp);
      id = nullChecker.value["id"];
      dbRef.update({"id": id + 1});
    }
    if (addToToday) {
      if (nullChecker2.value == null) {
        Map mp = {
          "device": device,
          "duration": int.parse(duration),
          "scheduled": true,
          "sid": id
        };

        dbRef2.child("0").set(mp);
        dbRef2.update({"id": 1});
      } else {
        Map mp = {
          "device": device,
          "duration": int.parse(duration),
          "scheduled": true,
          "sid": id
        };
        id2 = nullChecker2.value["id"];
        dbRef2.child(nullChecker2.value["id"].toString()).set(mp);
        dbRef2.update({"id": nullChecker2.value["id"] + 1});
      }
      dbRef.child(id.toString()).update({
        "instances": {startDate: id2}
      });
    }

    GreenCapScheduler.catchUpWholeSchedule();
  }
}

class Entry {
  int month;
  int year;
  int energy;
  int day;

  Entry(this.month, this.year, this.energy, this.day);
}
