import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:greencap/screens/screens.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'dart:async';

class AddDevicesScreen extends StatefulWidget {
  @override
  _AddDevicesScreenState createState() => _AddDevicesScreenState();
}

class _AddDevicesScreenState extends State<AddDevicesScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;
  String _category = "All";
  double _width;
  bool _checked = false;

  _AddDevicesScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));

    await db.once().then((value) => {ds = value});

    db.onValue.listen((event) {
      setState(() {
        db = db;
        ds = event.snapshot;
      });
    });
    setState(() {});
  }

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  createErrorDialog(BuildContext context, String error) {
    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          // StatefulBuilder
          builder: (context, setState) {
            return AlertDialog(
              actions: <Widget>[
                Container(
                    width: 400,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Error",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 2,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(error),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Material(
                              elevation: 5.0,
                              color: Colors.blue[900],
                              child: MaterialButton(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                                onPressed: () {
                                  setState(() {
                                    Navigator.of(context).pop();
                                  });
                                },
                                child: Text("Ok",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    )),
                              ),
                            ),
                          ],
                        )
                      ],
                    ))
              ],
            );
          },
        );
      },
    );
  }

  createAlertDialog(BuildContext context, List<Device> deviceList) {
    // Device testDevice = new Device("ID", "Test Wifi Plug", "Test MDL(AU)");
    // deviceList.add(testDevice);

    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          // StatefulBuilder
          builder: (context, setState) {
            return AlertDialog(
              actions: <Widget>[
                Container(
                    width: 400,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Add the following devices?",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 2,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        for (var i in deviceList)
                          ListTile(
                            title: Text(i.deviceName),
                            subtitle: Text("Model: " + i.deviceModel),
                            leading: Icon(Icons.devices),
                          ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Material(
                              elevation: 5.0,
                              color: Colors.blue[900],
                              child: MaterialButton(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                                onPressed: () {
                                  setState(() {
                                    Navigator.of(context).pop();
                                  });
                                },
                                child: Text("No",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    )),
                              ),
                            ),
                            Material(
                              elevation: 5.0,
                              color: Colors.blue[900],
                              child: MaterialButton(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                                onPressed: () {
                                  addDeviceAccountRow(emailController.text,
                                      passwordController.text, deviceList);
                                  setState(() {
                                    Navigator.of(context).pop();
                                  });
                                },
                                child: Text("Yes",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    )),
                              ),
                            ),
                          ],
                        )
                      ],
                    ))
              ],
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    if (db == null) {
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    } else {
      return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Color(0xFF053a54),
          title: Image.asset(
            'lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.only(top: 10, left: _width * 0.05 * 0.5),
                width: _width * 0.95,
                child: Column(children: [
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      width: _width * 0.95,
                      child: Text("Adding Devices",
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.w600,
                              fontSize: 28,
                              color: Color(0xFF053A54)))),
                  TextFormField(
                    controller: emailController,
                    obscureText: false,
                    decoration: const InputDecoration(
                      labelText: 'Email',
                    ),
                    validator: (String value) {
                      if (value.trim().isEmpty) {
                        return 'Email is required';
                      }
                    },
                  ),
                  TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: const InputDecoration(
                      labelText: 'Password',
                    ),
                    validator: (String value) {
                      if (value.trim().isEmpty) {
                        return 'Password is required';
                      }
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: _width * 0.95,
                    child: RaisedButton(
                      onPressed: () async {
                        var status = await _LoginStatus(
                            emailController.text, passwordController.text);
                        print(status);

                        if (status == 200) {
                          var errorCode = await _LoginErrorCode(
                              emailController.text, passwordController.text);
                          print(errorCode);
                          if (errorCode == 0) {
                            var token = await _LoginToken(
                                emailController.text, passwordController.text);
                            print(token);
                            List<Device> deviceList =
                                await _GetDeviceList(token);
                            deviceList.forEach((device) {
                              // Add Cards for each device
                              print(device.deviceName);
                              createAlertDialog(context, deviceList);
                            });
                          } else {
                            var errorMessage = await _LoginErrorMessage(
                                emailController.text, passwordController.text);
                            print(errorMessage);
                            createErrorDialog(context, errorMessage);
                          }
                        } else {
                          // Error Out
                          print("Status not 200 error");
                        }
                      },
                      child: const Text('Look for my Devices',
                          style: TextStyle(fontSize: 20)),
                    ),
                  ),
                ]))),
      );
    }
  }

  void addDeviceAccountRow(
      String email, String password, List<Device> deviceList) async {
    DatabaseReference dbRef = db.child("deviceAccount");
    DataSnapshot nullChecker = await dbRef.once();
    // print('Data : ${nullChecker.value}');
    if (nullChecker.value == null) {
      // print("Add Data");
      db.child("deviceAccount").set(
          {'Email': emailController.text, 'Password': passwordController.text});
      DatabaseReference dbRef = db.child("deviceAccount");
      for (var i = 0; i < deviceList.length; i++) {
        dbRef.child("$i").set({
          'DeviceId': deviceList[i].deviceId,
          'DeviceModel': deviceList[i].deviceModel,
          'DeviceName': deviceList[i].deviceName
        });
      }
    } else {
      setState(() {
        Navigator.of(context).pop();
      });
      createErrorDialog(context, "Devices Already Exist");
    }
  }
}

_LoginStatus(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the status code
  int statusCode = response.statusCode;
  return statusCode;
}

_LoginErrorCode(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  int errorCode = map['error_code'];
  return errorCode;
}

_LoginErrorMessage(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  String errorMessage = map['msg'];
  return errorMessage;
}

_LoginToken(String cloudUserName, String cloudPassword) async {
  // set up POST request arguments
  String url = 'https://wap.tplinkcloud.com';
  Map<String, String> headers = {"Content-type": "application/json"};
  String json =
      '{"method": "login","params": {"appType": "Kasa_Android","cloudUserName": "' +
          cloudUserName +
          '","cloudPassword": "' +
          cloudPassword +
          '","terminalUUID": "c2e0da23-474d-4b29-ade9-fa1baa33c0fe"}}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  String token = map['result']["token"];
  return token;
}

// Probably should have a method going here that checks the user has atleast one device. Yolo.

_GetDeviceList(String token) async {
  // set up POST request arguments
  String url = "https://wap.tplinkcloud.com?token=$token";
  Map<String, String> headers = {"Content-type": "application/json"};
  String json = '{"method":"getDeviceList"}';
  // make POST request
  Response response = await post(url, headers: headers, body: json);
  // Stores the response
  String body = response.body;
  Map<String, dynamic> map = jsonDecode(body);
  int numDevices = map['result']['deviceList'].length;
  List<Device> deviceList = [];
  for (var i = 0; i < numDevices; i++) {
    String deviceId = map['result']['deviceList'][i]['deviceId'];
    String deviceName = map['result']['deviceList'][i]['deviceName'];
    String deviceModel = map['result']['deviceList'][i]['deviceModel'];
    deviceList.add(Device(deviceId, deviceName, deviceModel));
    //print(deviceList[i].deviceId);
  }
  return deviceList;
}

class Device {
  String deviceId;
  String deviceName;
  String deviceModel;

  Device(this.deviceId, this.deviceName, this.deviceModel);
}
