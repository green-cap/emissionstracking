import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/firebase_database.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;
  Map<dynamic, dynamic> results;
  _HomeScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));
    //Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

    DatabaseReference devicesDbRef =
        FirebaseDatabase.instance.reference().child("devices");
    DataSnapshot deviceDs = await devicesDbRef.once();

    DatabaseReference unitDbRef =
        FirebaseDatabase.instance.reference().child("units");
    DataSnapshot unitDs = await unitDbRef.once();
    await db.once().then((value) => {ds = value});

    db.onValue.listen((event) {
      if (mounted) {
        setState(() {
          db = db;
          ds = event.snapshot;
        });
      }
    });

    //print("DID THIS HAPPEN BELOW HERE ON LOAD?");
    results = conversion(db, ds, unitDs, deviceDs);
    // print(results);
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    var accessibility = Icons.accessibility;
    double chartsize = 100;

    if ((db == null) || (results == null)) {
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    }

    return Scaffold(
        backgroundColor: Color(0xFF053a54),
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Color(0xFF053a54),
          title: Image.asset(
            'lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,
          ),
          centerTitle: true,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              // For the heights, ideally i would want height / 5. then split, 2, 1, 1, 1. But cant get the bottom nav bar to be included in the height
              padding: EdgeInsets.all(0.0),
              height: (MediaQuery.of(context).size.height -
                      AppBar().preferredSize.height) /
                  6 *
                  2,
              width: MediaQuery.of(context).size.width,
              child: Card(
                margin: EdgeInsets.all(0.0),
                color: Color(0xFF085f95),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: (MediaQuery.of(context).size.height -
                                AppBar().preferredSize.height) /
                            6 *
                            2,
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment(-0.8, -0.2),
                              child: Text(
                                'TOTAL',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                              ),
                            ),
                            Align(
                              alignment: Alignment(-0.8, 0.2),
                              child: Text(
                                double.parse(results["convertedTotal"]
                                        .toDouble()
                                        .toString())
                                    .toStringAsFixed(2),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                              ),
                            ),
                            Align(
                              alignment: Alignment(-0.75, 0.4),
                              child: Text(
                                results['units'],
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                              ),
                            ),
                            Container(
                              transform:
                                  Matrix4.translationValues(80.0, 0.0, 0.0),
                              child: DonutPieChart.withData(results),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ),
            Container(
              height: (MediaQuery.of(context).size.height -
                      AppBar().preferredSize.height) /
                  6,
              width: MediaQuery.of(context).size.width,
              child: Card(
                margin: EdgeInsets.all(0.0),
                color: Color(0xFF2199e2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      leading: Icon(
                        Icons.home,
                        size: 50,
                        color: Colors.white,
                      ),
                      title: Text(
                          (double.parse(results["electricityConverted"]
                                      .toDouble()
                                      .toString())
                                  .toStringAsFixed(2)
                                  .toString() +
                              " " +
                              results['units']),
                          style: TextStyle(color: Colors.white)),
                      //subtitle: Text('More Info.'),
                      //trailing: Icon(Icons.arrow_right, size: 50),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: (MediaQuery.of(context).size.height -
                      AppBar().preferredSize.height) /
                  6,
              width: MediaQuery.of(context).size.width,
              child: Card(
                margin: EdgeInsets.all(0.0),
                color: Color(0xFF5fbff9),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      leading: Icon(
                        Icons.directions_car,
                        size: 50,
                        color: Colors.white,
                      ),
                      title: Text(
                        (double.parse(results["transportConverted"]
                                    .toDouble()
                                    .toString())
                                .toStringAsFixed(2)
                                .toString() +
                            " " +
                            results['units']),
                        style: TextStyle(color: Colors.white),
                      ),
                      //subtitle: Text('More Info.'),
                      //trailing: Icon(Icons.arrow_right, size: 50),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: (MediaQuery.of(context).size.height -
                      AppBar().preferredSize.height) /
                  6,
              width: MediaQuery.of(context).size.width,
              child: Card(
                margin: EdgeInsets.all(0.0),
                color: Color(0xFF7dd5f8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      leading: Icon(
                        Icons.fastfood,
                        size: 50,
                        color: Colors.white,
                      ),
                      title: Text(
                        (double.parse(results["foodConverted"]
                                    .toDouble()
                                    .toString())
                                .toStringAsFixed(2)
                                .toString() +
                            " " +
                            results['units']),
                        style: TextStyle(color: Colors.white),
                      ),
                      //subtitle: Text(''),
                      //trailing: Icon(Icons.arrow_right, size: 50),
                    )
                  ],
                ),
              ),
            ),
            /*
            Container(
                height: (MediaQuery.of(context).size.height -
                        AppBar().preferredSize.height) /
                    20,
                width: MediaQuery.of(context).size.width,
                child: DotsIndicator(
                  dotsCount: 3,
                  position: 1,
                )),*/
          ],
        ));
  }

  double getTotalCO2(time, deviceName, deviceDs, type) {
    double total = 0.0;
    Map<dynamic, dynamic> deviceMap = deviceDs.value;

    Map<dynamic, dynamic> devices = deviceMap[type];

    //time is in minutes - convert to hour
    int time1 = time;

    double timeHours;
    bool found = false;

    //if type is food it is not actually time but grams - convert this to kg
    if (type == "Food") {
      timeHours = time1.toDouble() / 1000;
    } else {
      timeHours = time1.toDouble() / 60;
    }

    //find our device inside this set
    //THIS COULD BE IMPROVED FOR PERFORMANCE BUT I JUST NEED IT WORKING
    devices.forEach((key, value) {
      //check this entry and see if it is the correct device
      //wrap in try catch in case for some issue at the record level
      if (found == false) {
        try {
          if (value is! int && value is! String) {
            //this is a entry so maybe

            if (value['name'] == deviceName) {
              //found it

              double emission = value['factor'].toDouble();

              total = total + (emission * timeHours);

              found = true;
              //now we found the thingy that we want and changed it into total co2 emission
              return total;
            }
          }
        } catch (e) {
          print("some error happened lol " + e.toString());
        }
      }
    });
    return total;
  }

  double getUnitDisplay(totalEmissions, unit, unitDs) {
    //convert the total emissions into the unit
    //print(unitDs.value[unit]);
    double value = unitDs.value[unit].toDouble();
    double converted = 0.0;

    converted = totalEmissions / value;

    return converted;
  }

  double getSmartPlugCo2(Map<dynamic, dynamic> plugData) {
    double total = 0.0;
    //Map<dynamic, dynamic> plugStuff = plugData;

    //plugData is the child of deviceAccount which contains all the emissions
    try {
      plugData.forEach((key, value) {
        //check if the value is not a map or if key is 0
        if (key == '0' || value is int || value is String) {
          //print("skipped this");
        } else {
          //print(key.toString() + value.toString());
          double amount = value["Energy"].toDouble();
          //print(amount);

          total = total + amount;
        }
      });
      //print("The smart device total emissions in kw" + total.toString());
      total = (total / 1000) * 0.707;

      //print("The smart device total emissions in co2: " + total.toString());
    } catch (e) {
      print("error in smart plug");
    }
    return total;
  }

  Map<dynamic, dynamic> conversion(DatabaseReference db, DataSnapshot ds,
      DataSnapshot unitDs, DataSnapshot deviceDs) {
    //SharedPreferences prefs = await _prefs;
    //print("HI");
    //print(db.once());
    //print(ds.value);

    Map<dynamic, dynamic> map = ds.value;
    //print(map);
    //print(prefs.getString("uid"));

    //double carTotal = 0.0;
    double transportTotal = 0.0;
    double foodTotal = 0.0;
    double electrictyTotal = 0.0;
    String preferredUnit;
    double smartPlugCo2;

    map.forEach((key, value) {
      //print(key);
      if (key == 'schedule') {
        //print("scheduled so skip this");
      } else if (key == 'deviceAccount') {
        //this is the smart plug emissions data
        smartPlugCo2 = getSmartPlugCo2(value);
        //print("Smart plug total co2" + smartPlugCo2.toString());
        electrictyTotal = electrictyTotal + smartPlugCo2;
        //print("electricity total " + electrictyTotal.toString());
      } else if (key == 'user') {
        //get the preffered unit out here
        //print("IN USERRRR");
        preferredUnit = value['units']; //map.values.toList()[3];
        //print("PREFFERED UNIT: " + preferredUnit);
      } else {
        //print("YES");
        //print(value);

        //the value will be a map here
        Map<dynamic, dynamic> currentMap = value;
        currentMap.forEach((key, value) {
          //here we have the three types of Electricity, food and transport
          //for now just handle cars and their transport cost
          String type = key;

          Map<dynamic, dynamic> nextMap = value;
          nextMap.forEach((key, value) {
            //now we are each individual transport record on this date
            //these individuals records are also maps
            if (value is int || value is String) {
              //print("Did not do");
            } else {
              Map<dynamic, dynamic> currentRecord = value;
              //get the transport value
              double co2 = getTotalCO2(
                  value["duration"], value["device"], deviceDs, type);
              //print("SUCCESSFULLY FOUND: " + co2.toString());
              switch (type) {
                case "Electricity":
                  {
                    electrictyTotal = electrictyTotal + co2;
                  }
                  break;
                case "Transport":
                  {
                    transportTotal = transportTotal + co2;
                  }
                  break;
                case "Food":
                  {
                    foodTotal = foodTotal + co2;
                  }
                  break;
              }
            }
          });
        });
      }
    });

    double totalEmissions = electrictyTotal + foodTotal + transportTotal;
    double totalUnits = getUnitDisplay(totalEmissions, preferredUnit, unitDs);
    //print("new val to check " + lightBulbsUsed.toString());
    double electricityConverted =
        getUnitDisplay(electrictyTotal, preferredUnit, unitDs);
    double foodConverted = getUnitDisplay(foodTotal, preferredUnit, unitDs);
    double transportConverted =
        getUnitDisplay(transportTotal, preferredUnit, unitDs);
    Map<dynamic, dynamic> results = {
      "totalEmissions": totalEmissions,
      "electricityTotal": electrictyTotal,
      "foodTotal": foodTotal,
      "transportTotal": transportTotal,
      "units": preferredUnit,
      "convertedTotal": totalUnits,
      "electricityConverted": electricityConverted,
      "foodConverted": foodConverted,
      "transportConverted": transportConverted
    };
    return results;
  }
}

/*
Container(
                height: (MediaQuery.of(context).size.height -
                        AppBar().preferredSize.height) /
                    20,
                width: MediaQuery.of(context).size.width,
                child: DotsIndicator(
                  dotsCount: 3,
                  position: 1,
                )),

*/

/// Donut chart example. This is a simple pie chart with a hole in the middle.

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutPieChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory DonutPieChart.withData(results) {
    return new DonutPieChart(
      _createData(results),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        animate: animate,
        // Configure the width of the pie slices to 60px. The remaining space in
        // the chart will be left as a hole in the center.
        defaultRenderer: new charts.ArcRendererConfig(
            arcWidth: 60,
            arcRendererDecorators: [new charts.ArcLabelDecorator()]));
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createData(results) {
    String transport = (double.parse(
            (results["transportConverted"] * 100).toDouble().toString())
        .toStringAsFixed(0)
        .toString());

    String food =
        (double.parse((results["foodConverted"] * 100).toDouble().toString())
            .toStringAsFixed(0)
            .toString());

    String electricity = (double.parse(
            (results["electricityConverted"] * 100).toDouble().toString())
        .toStringAsFixed(0)
        .toString());

    final data = [
      //new LinearSales(0, int.parse(transport)),
      //new LinearSales(1, int.parse(food)),
      //new LinearSales(2, int.parse(electricity)),

      new LinearSales(0, int.parse(electricity)),
      new LinearSales(1, int.parse(transport)),
      new LinearSales(2, int.parse(food)),
      //new LinearSales(3, 5),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
        labelAccessorFn: (LinearSales row, _) {
          if (row.year == 0) {
            return "Elec.";
          } else if (row.year == 1) {
            return "Transp.";
          } else {
            return "Food";
          }
        },
      )
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
