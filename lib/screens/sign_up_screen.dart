import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:greencap/screens/screens.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/firebase_database.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final password1Controller = TextEditingController();
  final emailController = TextEditingController();
  final password2Controller = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;

  _SignUpScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    db = FirebaseDatabase.instance.reference();

    await db.once().then((value) => {ds = value});

    db.onValue.listen((event) {
      setState(() {
        db = db;
        ds = event.snapshot;
      });
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
        appBar: AppBar(
          leading: BackButton(
            onPressed: () => Navigator.pop(context),
          ),
          brightness: Brightness.light,
          backgroundColor: const Color(0xFF053A54),
          title: Image.asset(
            'lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,
          ),
          centerTitle: true,
        ),
        body: Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 60.0,
              ),
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 5),
                      width: width * 0.85,
                      child: Text("Sign Up",
                          style: TextStyle(
                              fontFamily: "Avenir",
                              fontWeight: FontWeight.w600,
                              fontSize: 30,
                              color: Color(0xFF053A54)))),
                  Center(
                      child: Container(
                          width: width * 0.85,
                          margin: EdgeInsets.only(top: 15),
                          child: TextField(
                            controller: firstNameController,
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Avenir',
                                fontSize: 18),
                            decoration: InputDecoration(
                              hintText: "Enter your first name",
                              hintStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 1),
                              labelText: "First Name",
                              labelStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 0.5),
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Color(0xFF00263d))),
                            ),
                          ))),
                  Center(
                      child: Container(
                          width: width * 0.85,
                          margin: EdgeInsets.only(top: 15),
                          child: TextField(
                            controller: lastNameController,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Avenir',
                              fontSize: 18,
                            ),
                            decoration: InputDecoration(
                              hintText: "Enter your last name",
                              hintStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 1),
                              labelText: "Last Name",
                              labelStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 0.5),
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Color(0xFF00263d))),
                            ),
                          ))),
                  Center(
                      child: Container(
                          width: width * 0.85,
                          margin: EdgeInsets.only(top: 15),
                          child: TextField(
                            controller: emailController,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Avenir',
                              fontSize: 18,
                            ),
                            decoration: InputDecoration(
                              hintText: "Enter your email address",
                              hintStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 1),
                              labelText: "Email Address",
                              labelStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 0.5),
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Color(0xFF00263d))),
                            ),
                          ))),
                  Center(
                      child: Container(
                          width: width * 0.85,
                          margin: EdgeInsets.only(top: 15),
                          child: TextField(
                            obscureText: true,
                            controller: password1Controller,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Avenir',
                              fontSize: 18,
                            ),
                            decoration: InputDecoration(
                              hintText: "Enter your password",
                              hintStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 1),
                              labelText: "Password",
                              labelStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 0.5),
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Color(0xFF00263d))),
                            ),
                          ))),
                  Center(
                      child: Container(
                          width: width * 0.85,
                          margin: EdgeInsets.only(top: 15),
                          child: TextField(
                            obscureText: true,
                            controller: password2Controller,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Avenir',
                              fontSize: 18,
                            ),
                            decoration: InputDecoration(
                              hintText: "Retype your password",
                              hintStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 1),
                              labelText: "Retyped Password",
                              labelStyle: new TextStyle(
                                  color: Color(0xFF00263d), height: 0.5),
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Color(0xFF00263d))),
                            ),
                          ))),
                  _buildSignupButton(context)
                ],
              ),
            )));
  }

  Center _buildSignupButton(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 30),
        child: RaisedButton(
          color: Color(0xFF05CC7F),
          elevation: 5.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            //Navigator.pop(context);
            _signUpProcess(context);
          },
          child: Text("SIGN UP",
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 1.5,
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.w900,
                  fontSize: 25)),
        ),
        width: 0.4 * width,
        height: 50.0,
      ),
    );
  }

  _signUpProcess(context) async {
    //First check that passwords match
    if (password1Controller.text != password2Controller.text) {
      Toast.show("Passwords do not match", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
      return;
    }

    //now create this user
    try {
      UserCredential newUserCreds = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: emailController.text, password: password1Controller.text);
      print("REGISTERED NEW ACCOUNT");
      User newUser = newUserCreds.user;

      print(newUser.email);
      newUser.updateProfile();

      newUser.updateProfile(
          displayName: "$firstNameController + $lastNameController");

      //if at this point then account is made :)
      Toast.show("Account registered!", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);

      final SharedPreferences prefs = await _prefs;
      prefs.setBool("isLoggedIn", true);
      prefs.setString("username", newUser.displayName); // is their email
      prefs.setString("uid", newUser.uid);

      Map map = {
        "first": firstNameController.text,
        "last": lastNameController.text,
        "email": emailController.text,
        "units": "lightbulb"
      };

      db.child(newUser.uid).child("user").set(map);

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => AppScreen()),
          (Route<dynamic> route) => false);
    } catch (e) {
      print("Error: $e");

      //display the error
      var message = e.toString().split('] ')[1];
      print("Message part: $message");
      Toast.show(message, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
    }
  }
}
