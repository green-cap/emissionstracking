import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:greencap/screens/screens.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:greencap/main.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final passwordController = TextEditingController();
  final usernameController = TextEditingController();

  //Used for setting local key value pairs for persistent logging in
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    final Row orDivider = Row(
      children: [
        SizedBox(height: height * .1),

        Expanded(
            child: Divider(
          color: Colors.white,
          height: 20,
          thickness: 2,
        )),
        Padding(
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Text(
              "OR",
              style: TextStyle(
                color: Colors.white,
                fontFamily: "Avenir",
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            )),
        Expanded(
            child: Divider(
          color: Colors.white,
          height: 20,
          thickness: 2,
        )),
        //SizedBox(height: height * .05),
      ],
    );

    return Scaffold(
        body: Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF053A54),
                Color(0xFF00263D),
              ],
              stops: [0.5, 1],
            ),
          ),
        ),
        Container(
          height: double.infinity,
          child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 60.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  //Logo
                  Center(
                    child: Container(
                      child: Image(
                          image: AssetImage('lib/assets/greencap.png'),
                          width: width * 0.6),
                    ),
                  ),
                  //Spacer
                  SizedBox(
                    height: 0.02 * height,
                  ),
                  // Username TextField
                  _buildTextField("Email", Icon(Icons.person_outline), false,
                      usernameController),
                  SizedBox(
                    height: 0.03 * height,
                  ),
                  // Password TextField
                  _buildTextField("Password", Icon(Icons.lock_outline), true,
                      passwordController),

                  SizedBox(
                    height: 0.03 * height,
                  ),
                  // Login button
                  _buildLoginButton(context),
                  orDivider,
                  FacebookSignInButton(
                      onPressed: () {
                        // call authentication logic
                      },
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontFamily: "Avenir",
                        fontWeight: FontWeight.w500,
                        fontSize: 17,
                      )),
                  GoogleSignInButton(
                      onPressed: () {/* ... */},
                      darkMode: false, // default: false
                      splashColor: Colors.white,
                      textStyle: TextStyle(
                        color: Colors.black,
                        fontFamily: "Avenir",
                        fontWeight: FontWeight.w500,
                        fontSize: 17,
                      )),
                  SizedBox(height: 10),
                  Center(
                      child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'Don\'t have an account?  ',
                            style: TextStyle(
                                fontFamily: "Avenir",
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.2)),
                        TextSpan(
                          text: 'Sign up!',
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignUpScreen()));
                            },
                          style: TextStyle(
                              fontFamily: "Avenir",
                              color: Color(0xFF62CBF9),
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5),
                        )
                      ],
                    ),
                  )),
                  SizedBox(height: 10),
                  Center(
                      child: Text("Forgot your Password?",
                          style: TextStyle(
                              fontFamily: "Avenir",
                              color: Color(0xFF62CBF9),
                              fontWeight: FontWeight.w600)))
                ],
              )),
        )
      ],
    ));
  }

  Center _buildLoginButton(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Center(
      child: Container(
        child: RaisedButton(
          color: Color(0xFF05CC7F),
          elevation: 5.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            _setLoginPrefs(context, usernameController.text);
          },
          child: Text("LOG IN",
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 1.5,
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.w900,
                  fontSize: 25)),
        ),
        width: 0.4 * width,
        height: 50.0,
      ),
    );
  }

  _setLoginPrefs(BuildContext context, String username) async {
    print("Testing button email: " + usernameController.text);
    print('password: ' + passwordController.text);
    if (usernameController.text == null || usernameController.text == "") {
      Toast.show("Please enter in an email", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
      return;
    } else if (passwordController.text == null ||
        passwordController.text == "") {
      Toast.show("Please enter in a password", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
      return;
    }
    //DO BACKEND CHECKING HERE
    User userAccount;
    //print("1: " + userAccount.emailVerified.toString());
    try {
      UserCredential validateUser = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: usernameController.text,
              password: passwordController.text);

      userAccount = validateUser.user;
      final SharedPreferences prefs = await _prefs;
      prefs.setBool("isLoggedIn", true);
      prefs.setString("username", userAccount.displayName); // is their email
      prefs.setString("uid", userAccount.uid);

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => AppScreen()),
          (Route<dynamic> route) => false);
    } catch (e) {
      print("Error: $e");
      var message = e.toString().split('] ')[1];
      print("Message part: $message");
      Toast.show(message, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
    }
    return;
  }

  Row _buildTextField(
      String hintText, Icon icon, bool obs, TextEditingController tec) {
    return Row(children: [
      Expanded(
        child: TextField(
          obscureText: obs,
          controller: tec,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Avenir',
            fontSize: 20,
          ),
          decoration: InputDecoration(
              hintText: hintText,
              hintStyle: TextStyle(
                  fontFamily: 'Avenir',
                  fontWeight: FontWeight.w300,
                  fontSize: 20),
              prefixIcon: icon,
              border: new OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: const BorderRadius.all(
                  const Radius.circular(5.0),
                ),
              ),
              fillColor: Colors.white,
              filled: true),
        ),
      ),
    ]);
  }
}
