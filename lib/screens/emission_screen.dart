import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ml_algo/ml_algo.dart';
import 'package:ml_dataframe/ml_dataframe.dart';

class EmissionScreen extends StatefulWidget {
  @override
  _EmissionScreenState createState() => _EmissionScreenState();
}


class _EmissionScreenState extends State<EmissionScreen> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DatabaseReference db;
  DataSnapshot ds;
  DataSnapshot deviceDs;

  bool elec = true;
  bool trans = false;
  bool food = false;
  _EmissionScreenState() {
    getDb();
  }

  getDb() async {
    SharedPreferences prefs = await _prefs;
    //print(prefs.getString("uid"));
    db = FirebaseDatabase.instance.reference().child(prefs.getString("uid"));
    DatabaseReference devicesDbRef = FirebaseDatabase.instance.reference().child("devices");
    deviceDs = await devicesDbRef.once();
    await db.once().then((value) => {ds = value});
    db.onValue.listen((event) {
      if (mounted)  {
        setState(() {
          db = db;
          ds = event.snapshot;
        });  
      }
    });
    //print(ds.value);

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    if (db == null){
      return Scaffold(
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Color(0xFF053a54),
            title: Image.asset(
              'lib/assets/greencap.png',
              width: 80,
              fit: BoxFit.fitWidth,
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Text("Loading...",
                style: TextStyle(
                    fontSize: 25, fontFamily: "Avenir", color: Colors.black)),
          ));
    }
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Color(0xFF053a54),
        title: Image.asset('lib/assets/greencap.png',
            width: 80,
            fit: BoxFit.fitWidth,),
            centerTitle: true,
      ),


      backgroundColor: Color(0xFFf6f6f6),
      body:
      ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget> [
            GraphSpace(ds: ds, deviceDs: deviceDs),
            PredictionSpace(ds: ds, deviceDs: deviceDs),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Suggestions", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Color(0xFF053a54))),

              ],
            ),
           checkHighestType(ds.value, deviceDs, 'Electricity') ? Text("\n \u2022 Turn off light bulbs after use.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Color(0xFF507587))
           ): new Container(),
           checkHighestType(ds.value, deviceDs, 'Electricity') ? Text("\n \u2022 Consider switching to LED lights.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Color(0xFF507587))
           ): new Container(),
           checkHighestType(ds.value, deviceDs, 'Electricity') ? Text("\n \u2022 Only use necessary house appliances.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Color(0xFF507587))
           ): new Container(),
           checkHighestType(ds.value, deviceDs, 'Transport') ? Text("\n \u2022 Consider riding a bike or walking instead of driving.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Color(0xFF507587))
           ): new Container(),
           checkHighestType(ds.value, deviceDs, 'Food') ? Text("\n \u2022 Reduce consumption of steak.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Color(0xFF507587))
           ): new Container(),

        ],
      ),
    );


  }

  // gets the total
  double getTotals(dsVal, deviceDs, type) {

    double sum = 0.0;

    if (dsVal != null) {
      for (var date in dsVal.keys) {
        var value = dsVal[date];
        if (value.containsKey(type)) {
          var entries = value[type];
          for (var num in entries.keys) {

            var values = entries[num];
            if (!(values is int)) {
              if (values.containsKey('duration')) {
                int time = values['duration'].toInt();
                if (values.containsKey('duration')) {
                  int time = values['duration'].toInt();
                  if (values.containsKey('device')) {
                    var device = values['device'];
                    sum += getTotalCO2(time,device, deviceDs, type);
                  }

                }

              }
            }
          }
        }
      }
    }
    return sum;
  }

  bool checkHighestType(dsVal, deviceDs, type) {
    double totalElec = getTotals(dsVal, deviceDs,'Electricity');
    double totalTrans = getTotals(dsVal, deviceDs,'Transport');
    double totalFood = getTotals(dsVal, deviceDs,'Food');

    if (totalElec >= totalTrans && totalElec >= totalFood){
      if (type == 'Electricity') {
        return true;
      }
    } else if (totalTrans >= totalElec && totalTrans >= totalFood) {
      if (type == 'Transport') {
        return true;
      }
    } else {
      if (type == 'Food') {
        return true;
      }
    }
    return false;
  }

  double getTotalCO2(time, deviceName, deviceDs, type) {
    double total = 0.0;
    Map<dynamic, dynamic> deviceMap = deviceDs.value;


    Map<dynamic, dynamic> devices = deviceMap[type];
    //time is in minutes - convert to hour
    int time1 = time;
    double timeHours;
    if (type == "Food") {
      timeHours = time1.toDouble() / 1000;
    } else {
      timeHours = time1.toDouble() / 60;
    }
    bool found = false;


    //find our device inside this set

    devices.forEach((key, value) {

      //check this entry and see if it is the correct device
      //wrap in try catch in case for some issue at the record level
      if (found == false) {
        try {
          if (value is! int && value is! String) {
            //this is a entry so maybe

            if (value['name'] == deviceName) {
              //found it

              double emission = value['factor'].toDouble();
              total = total + (emission * timeHours);

              found = true;
              //now we found the thingy that we want and changed it into total co2 emission
              return total;
            }
          }
        } catch (e) {
          print("some error happened" + e.toString());
        }
      }
    });
    return total;
  }
}


// ----------------------------------- PREDICTIONS -----------------

class PredictionSpace extends StatefulWidget {
  final ds;
  final deviceDs;
  const PredictionSpace({Key key, this.ds, this.deviceDs}): super(key: key);
  @override
  PredictionSpaceState createState() => new PredictionSpaceState();
}

class PredictionSpaceState extends State<PredictionSpace> {
  bool visibility6Month = false;
  bool visibility5Day = true;
  String val = "Next 5 Days";

  @override
  Widget build(BuildContext context){


    return Column(
        children: <Widget> [

          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Predictions", style: TextStyle(fontWeight: FontWeight.w600,fontFamily: "Avenir", fontSize: 24, color: Color(0xFF053a54))),
              ]
          ),

          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              DropdownButton<String>(
                isExpanded: false,
                value: val,
                icon: Icon(Icons.arrow_downward, color: Color(0xFF053a54)),
                style: TextStyle(color: Color(0xFF053a54)),
                onChanged: (String newVal) {
                  setState(() {
                    val = newVal;
                    if (newVal == "Next 6 Months") {
                      visibility6Month = true;
                      visibility5Day = false;
                    }
                    if (newVal == "Next 5 Days"){
                      visibility6Month = false;
                      visibility5Day = true;
                    }
                  });
                },

                iconSize: 20,

                items: <String>["Next 5 Days", "Next 6 Months"].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(value:value, child: Text(value, style: TextStyle(fontWeight: FontWeight.w600,fontFamily: "Avenir", fontSize: 20, color: Color(0xFF053a54))));}).toList(),

              ),
            ],
          ),
          visibility5Day ? EmissionsPredictorLine(
            //data:dataLine,
            data: get5DaysPredictions(widget.ds.value,widget.deviceDs),
            title: "Next 5 Days Predicted Emissions",
          ): new Container(),
          visibility6Month ? EmissionsPredictorLine(
            data: get6MonthPredictions(widget.ds.value,widget.deviceDs),
            title: "Next 6 Months Predicted Emissions",
          ): new Container(),
        ] );
  }

  String getFormattedDate(date) {
    return "${date.day.toString().padLeft(2, '0')}-${date.month.toString().padLeft(2, '0')}-${date.year.toString()}";
  }

  LinearRegressor mlLinearRegressionMonthly(dsVal) {

    List<EmissionsLineSeries> pastData = getPast12Months(widget.ds.value, widget.deviceDs,true);

    // get the past 12 months data into a dataframe
    final past12Month = DataFrame(<Iterable<num>>[
      [1,pastData[11].amounts],
      [2,pastData[10].amounts],
      [3,pastData[9].amounts],
      [4,pastData[8].amounts],
      [5,pastData[7].amounts],
      [6,pastData[6].amounts],
      [7,pastData[5].amounts],
      [8,pastData[4].amounts],
      [9,pastData[3].amounts],
      [10,pastData[2].amounts],
      [11,pastData[1].amounts],
      [12,pastData[0].amounts],

    ], headerExists: false);

    //train the linear model
    final regression = LinearRegressor(
        past12Month, 'col_1',
        iterationsLimit: 100,
        initialLearningRate: 0.0005,
        learningRateType: LearningRateType.constant);

    // learned coefficients
    print('Regression coefficients: ${regression.coefficients}');

    return regression;
  }

  LinearRegressor mlLinearRegressionDaily(dsVal, deviceDs) {

    List<EmissionsLineSeries> pastData = getPast5Days(widget.ds.value, deviceDs, true);

    // get the past 12 months data into a dataframe
    final past5Days = DataFrame(<Iterable<num>>[
      [1,pastData[4].amounts],
      [2,pastData[3].amounts],
      [3,pastData[2].amounts],
      [4,pastData[1].amounts],
      [5,pastData[0].amounts],


    ], headerExists: false);

    //train the linear model
    final regression = LinearRegressor(
        past5Days, 'col_1',
        iterationsLimit: 100,
        initialLearningRate: 0.0005,
        learningRateType: LearningRateType.constant);

    // learned coefficients
    print('Regression coefficients: ${regression.coefficients}');

    return regression;
  }

  // gets the next 6 months predicted
  List<EmissionsLineSeries> get6MonthPredictions(dsVal, deviceDs) {
    List<EmissionsLineSeries> scheduledData = getScheduled6Months(widget.ds.value, deviceDs);
    // train and get resulting regression model
    final monthlyModel = mlLinearRegressionMonthly(widget.ds.value);

    // now predict using an averaged combination of model prediction based on
    // past data and scheduled emissions
    List<EmissionsLineSeries> predicted = [];
    DateTime now = new DateTime.now();
    for (var i = 0; i < 6; i++) {
      var scheduled = scheduledData[i];
      double scheduledAmount = scheduled.amounts;
      DataFrame prediction = monthlyModel.predict(DataFrame(<Iterable<num>>[
        [i+12]], headerExists: false));

      double predictedEmissions = prediction[0].data.first;
      double overallPrediction = (scheduledAmount + predictedEmissions)/2;
      print('Prediction for month $i is $overallPrediction');

      predicted.add(EmissionsLineSeries(
          date: new DateTime(now.year, now.month, now.day),
          amounts: overallPrediction));
      now = now.add(Duration(days: 30));

    }
    return predicted;
  }

  // gets the next 5 days predicted
  List<EmissionsLineSeries> get5DaysPredictions(dsVal, deviceDs) {
    List<EmissionsLineSeries> scheduledData = getScheduled5Days(widget.ds.value, widget.deviceDs);
    // train and get resulting regression model
    final monthlyModel = mlLinearRegressionDaily(dsVal,deviceDs);

    // now predict using an averaged combination of model prediction based on
    // past data and scheduled emissions
    List<EmissionsLineSeries> predicted = [];
    DateTime now = new DateTime.now();
    for (var i = 0; i < 5; i++) {
      var scheduled = scheduledData[i];
      double scheduledAmount = scheduled.amounts;
      DataFrame prediction = monthlyModel.predict(DataFrame(<Iterable<num>>[
        [i+5]], headerExists: false));

      double predictedEmissions = prediction[0].data.first;
      double overallPrediction = (scheduledAmount + predictedEmissions)/2;
      print('Prediction for day $i is $overallPrediction');

      predicted.add(EmissionsLineSeries(
          date: new DateTime(now.year, now.month, now.day),
          amounts: overallPrediction));
      now = now.add(Duration(days: 1));

    }
    return predicted;
  }

  // Gets the input data for the time series graph of past 5 days total emissions
  List<EmissionsLineSeries> getPast5Days(dsVal, deviceDs, user) {
    DateTime now = new DateTime.now();

    if (user) {
      List<EmissionsLineSeries> dataLine = [];
      for (var i = 0; i < 5; i++){
        dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: getEmissionsDate(dsVal, deviceDs, now)));
        now = now.subtract(Duration(days:1));

      }

      return dataLine;
    }
    else {
      List<EmissionsLineSeries> dataLine = [];
      for (var i = 0; i < 5; i++){
        dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: 100));
        now = now.subtract(Duration(days:1));

      }

      return dataLine;
    }
  }

  // gets the next 5 days scheduled
  List<EmissionsLineSeries> getScheduled5Days(dsVal, deviceDs) {
    DateTime now = new DateTime.now();


    List<EmissionsLineSeries> dataLine = [];
    for (var i = 0; i < 5; i++) {
      dataLine.add(EmissionsLineSeries(
          date: new DateTime(now.year, now.month, now.day),
          amounts: getEmissionsFutureDay(dsVal, deviceDs, now)));
      now = now.add(Duration(days: 1));
    }

    return dataLine;
  }

  // gets total scheduled emissions for a future day
  double getEmissionsFutureDay(dsVal, deviceDs, now) {
    double sum = 0.0;

    if (dsVal != null) {
      for (var date in dsVal.keys) {
        if ( date == 'schedule') {
          var value = dsVal[date];
          for (var scheduledEvent in value.keys) {
            var scheduledEventObj = value[scheduledEvent];
            if (!(scheduledEventObj.runtimeType == int)) {
              if (scheduledEventObj.containsKey('duration')) {
                if (scheduledEventObj.containsKey('freq')) {
                    //print(scheduledEventObj['device']);
                    int time = scheduledEventObj['duration'].toInt();

                    if (scheduledEventObj.containsKey('device')) {
                      var device = scheduledEventObj['device'];
                      sum += getTotalCO2(time,device, deviceDs, scheduledEventObj['type'])  /
                          scheduledEventObj['freq'].toDouble();
                    }

                  }

                }
              }
            }

          }

        }

      }




    return sum;
  }

  // gets the next 6 months scheduled
  List<EmissionsLineSeries> getScheduled6Months(dsVal, deviceDs) {
    DateTime now = new DateTime.now();
    //print(now);


    List<EmissionsLineSeries> dataLine = [];
    for (var i = 0; i < 6; i++) {
      dataLine.add(EmissionsLineSeries(
          date: new DateTime(now.year, now.month, now.day),
          amounts: getEmissionsFutureMonth(dsVal, deviceDs, now.month)));
      now = now.add(Duration(days: 30));
    }

    return dataLine;
  }

  // gets total emissions for a given month
  double getEmissionsMonth(dsVal, deviceDs, month) {
    double sum = 0.0;


    if (dsVal != null) {
      for (var date in dsVal.keys) {
        if (date != 'schedule' && date != 'user' && date != 'deviceAccount') {
          if (month == int.parse(date.substring(3, 5))) {
            var value = dsVal[date];
            for (var type in value.keys) {
              var entries = value[type];
              for (var num in entries.keys) {
                var values = entries[num];
                if (!(values is int)) {
                  if (values.containsKey('duration')) {
                    int time = values['duration'].toInt();
                    if (values.containsKey('device')) {
                      var device = values['device'];
                      sum += getTotalCO2(time,device, deviceDs, type);
                    }

                  }
                }
              }
            }
          }
        }
      }
    }
    return sum;
  }

  // Gets the input data for the time series graph of past 12 months total emissions
  List<EmissionsLineSeries> getPast12Months(dsVal, deviceDs, user) {
    DateTime now = new DateTime.now();

    if (user) {
      List<EmissionsLineSeries> dataLine = [];
      for (var i = 0; i < 12; i++){
        dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: getEmissionsMonth(dsVal, deviceDs, now.month)));
        now = now.subtract(Duration(days:30));
      }

      return dataLine;
    }
    else {
      List<EmissionsLineSeries> dataLine = [];
      for (var i = 0; i < 12; i++){
        dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: 100));
        now = now.subtract(Duration(days:30));
      }

      return dataLine;
    }

  }

  // gets total emissions for a given date
  double getEmissionsDate(dsVal, deviceDs, now) {
    double sum = 0.0;
    String formattedDate = getFormattedDate(now);

    if (dsVal != null) {
      for (var date in dsVal.keys) {
        if (date == formattedDate){
          var value = dsVal[date];
          for (var type in value.keys) {
            var entries = value[type];
            for (var num in entries.keys) {

              var values = entries[num];
              if (!(values is int)) {
                if (values.containsKey('duration')) {
                  int time = values['duration'].toInt();
                  if (values.containsKey('device')) {
                    var device = values['device'];
                    sum += getTotalCO2(time,device, deviceDs, type);
                  }

                }
              }
            }
          }
        }
      }
    }
    return sum;
  }

  // gets total emissions for a given future month
  double getEmissionsFutureMonth(dsVal, deviceDs, month) {
    double sum = 0.0;
    //print(dsVal);

    if (dsVal != null) {

      for (var date in dsVal.keys) {
        //print('lol');
        if (date == 'schedule') {
          var value = dsVal[date];
          for (var scheduledEvent in value.keys) {
            var scheduledEventObj = value[scheduledEvent];
            //print(scheduledEventObj);
            if (!(scheduledEventObj.runtimeType == int)) {
              int time = scheduledEventObj['duration'].toInt();
              if (scheduledEventObj.containsKey('device')) {
                var device = scheduledEventObj['device'];
                sum += getTotalCO2(time,device, deviceDs, scheduledEventObj['type']) *30  /
                    scheduledEventObj['freq'].toDouble();
              }
            }

          }

        }

      }

    }


    return sum;
  }
  double getTotalCO2(time, deviceName, deviceDs, type) {
    double total = 0.0;
    Map<dynamic, dynamic> deviceMap = deviceDs.value;


    Map<dynamic, dynamic> devices = deviceMap[type];
    //time is in minutes - convert to hour
    int time1 = time;
    double timeHours;
    if (type == "Food") {
      timeHours = time1.toDouble() / 1000;
    } else {
      timeHours = time1.toDouble() / 60;
    }
    bool found = false;


    //find our device inside this set

    devices.forEach((key, value) {

      //check this entry and see if it is the correct device
      //wrap in try catch in case for some issue at the record level
      if (found == false) {
        try {
          if (value is! int && value is! String) {
            //this is a entry so maybe

            if (value['name'] == deviceName) {
              //found it

              double emission = value['factor'].toDouble();
              total = total + (emission * timeHours);

              found = true;
              //now we found the thingy that we want and changed it into total co2 emission
              return total;
            }
          }
        } catch (e) {
          print("some error happened" + e.toString());
        }
      }
    });
    return total;
  }


}


// ----------------------------------- GRAPHS (PAST DATA) -----------------------

class GraphSpace extends StatefulWidget {
  final ds;
  final deviceDs;

  const GraphSpace({ Key key, this.ds, this.deviceDs }): super(key: key);

  @override
  GraphSpaceState createState() => new GraphSpaceState();
}

class GraphSpaceState extends State<GraphSpace> {

  bool visibilityShort = true;
  bool visibilityCum = false;
  bool visibilityTypeChart = false;

  String val = "Past 5 Days Emissions";

  @override
  Widget build(BuildContext context){
    return Column(
          children: <Widget> [

            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                Text("Emissions", style: TextStyle(fontWeight: FontWeight.w600,fontFamily: "Avenir", fontSize: 24, color: Color(0xFF053a54))),
                 ]
            ),

            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                    DropdownButton<String>(
                      isExpanded: false,
                      value: val,
                      icon: Icon(Icons.arrow_downward, color: Color(0xFF053a54)),
                      style: TextStyle(color: Color(0xFF053a54)),
                      onChanged: (String newVal) {
                        setState(() {
                          val = newVal;
                          if (newVal == "Past 5 Days Emissions"){
                            visibilityShort = true;
                            visibilityTypeChart = false;
                            visibilityCum = false;
                          }
                          if (newVal == "Emissions Per Category"){
                            visibilityTypeChart = true;
                            visibilityShort = false;
                            visibilityCum = false;
                          }
                          if (newVal == "Past 12 Months Emissions"){
                            visibilityCum = true;
                            visibilityTypeChart = false;
                            visibilityShort = false;
                          }
                        });
                    },

                    iconSize: 20,

                    items: <String>["Past 5 Days Emissions", "Emissions Per Category", "Past 12 Months Emissions"].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(value:value, child: Text(value, style: TextStyle(fontWeight: FontWeight.w600,fontFamily: "Avenir", fontSize: 20, color: Color(0xFF053a54))));}).toList(),

                    ),
                ],
            ),

            visibilityShort ? EmissionsLine(
              //data:dataLine,
              data: getPast5Days(widget.ds.value, widget.deviceDs, true),
              dataAus:getPast5Days(widget.ds.value, widget.deviceDs, false),
              title: "Past 5 Days Total Emissions",
            ): new Container(),
            visibilityCum ? EmissionsLine(
              data:getPast12Months(widget.ds.value, widget.deviceDs,true),
              dataAus:getPast12Months(widget.ds.value, widget.deviceDs, false),
              title: "Past 12 Months Emissions",
            ): new Container(),
            visibilityTypeChart ? EmissionsChart(
              data:[
                Emissions(emissionType: "Transport", amounts: getTotals(widget.ds.value, widget.deviceDs, 'Transport'), colour: charts.ColorUtil.fromDartColor(Color(0xFF229ae3))),
                Emissions(emissionType: "Electricity", amounts: getTotals(widget.ds.value, widget.deviceDs, 'Electricity'), colour: charts.ColorUtil.fromDartColor(Color(0xFF086096))),
                Emissions(emissionType: "Food", amounts: getTotals(widget.ds.value, widget.deviceDs, 'Food'), colour: charts.ColorUtil.fromDartColor(Color(0xFF5fbffa))),
              ],
            ): new Container(),



       ] );
  }

  String getFormattedDate(date) {
    return "${date.day.toString().padLeft(2, '0')}-${date.month.toString().padLeft(2, '0')}-${date.year.toString()}";
  }

  // gets total emissions for a given date
  double getEmissionsDate(dsVal, deviceDs, now) {
    double sum = 0.0;
    String formattedDate = getFormattedDate(now);

    if (dsVal != null) {
      for (var date in dsVal.keys) {
        if (date == formattedDate){
          var value = dsVal[date];
          for (var type in value.keys) {
            var entries = value[type];
            for (var num in entries.keys) {

              var values = entries[num];
              if (!(values is int)) {
                if (values.containsKey('duration')) {
                  int time = values['duration'].toInt();
                  if (values.containsKey('device')) {
                    var device = values['device'];
                    sum += getTotalCO2(time,device, deviceDs, type);
                  }

                }
              }
            }
          }
        }
      }
    }
    return sum;
  }

  // gets total emissions for a given month
  double getEmissionsMonth(dsVal, deviceDs, month) {
    double sum = 0.0;

    if (dsVal != null) {
      for (var date in dsVal.keys) {
        if (date != 'schedule' && date != 'user' && date != 'deviceAccount') {
          if (month == int.parse(date.substring(3, 5))) {
            var value = dsVal[date];
            for (var type in value.keys) {
              var entries = value[type];
              for (var num in entries.keys) {
                var values = entries[num];
                if (!(values is int)) {
                  if (values.containsKey('duration')) {
                    int time = values['duration'].toInt();
                    if (values.containsKey('device')) {
                      var device = values['device'];
                      sum += getTotalCO2(time,device, deviceDs, type);
                    }

                  }
                }
                }
              }
            }
          }
        }
      }

    return sum;
  }

double getTotalCO2(time, deviceName, deviceDs, type) {
  double total = 0.0;
  Map<dynamic, dynamic> deviceMap = deviceDs.value;
  // print("IN FUNCTION");
  // print(deviceMap);

  Map<dynamic, dynamic> devices = deviceMap[type];
  //time is in minutes - convert to hour
  int time1 = time;
  double timeHours;
  if (type == "Food") {
    timeHours = time1.toDouble() / 1000;
  } else {
    timeHours = time1.toDouble() / 60;
  }
  bool found = false;

  // print(deviceName);
  // print(devices);
  //find our device inside this set
  //THIS COULD BE IMPROVED FOR PERFORMANCE BUT I JUST NEED IT WORKING
  devices.forEach((key, value) {
    //print("This row is: " + key.toString() + value.toString());
    //check this entry and see if it is the correct device
    //wrap in try catch in case for some issue at the record level
    if (found == false) {
      try {
        if (value is! int && value is! String) {
          //this is a entry so maybe
          //print("Checking this ");
          if (value['name'] == deviceName) {
            //found it
            //print("FOUND IT SUCKA");
            double emission = value['factor'].toDouble();
            total = total + (emission * timeHours);
            // print(total.toString());
            found = true;
            //now we found the thingy that we want and changed it into total co2 emission
            return total;
          }
        }
      } catch (e) {
        print("some error happened lol" + e.toString());
      }
    }
  });
  return total;
}

  // Gets the input data for the time series graph of past 12 months total emissions
  List<EmissionsLineSeries> getPast12Months(dsVal,deviceDs, user) {
      DateTime now = new DateTime.now();

      if (user) {
        List<EmissionsLineSeries> dataLine = [];
        for (var i = 0; i < 12; i++){
          dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: getEmissionsMonth(dsVal, deviceDs, now.month)));
          now = now.subtract(Duration(days:30));
        }

        return dataLine;
      }
      else {
        List<EmissionsLineSeries> dataLine = [];
        for (var i = 0; i < 12; i++){
          dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: 166.66));
          now = now.subtract(Duration(days:30));
        }

        return dataLine;
      }

  }


  // Gets the input data for the time series graph of past 5 days total emissions
  List<EmissionsLineSeries> getPast5Days(dsVal, deviceDs, user) {
    DateTime now = new DateTime.now();

    if (user) {
      List<EmissionsLineSeries> dataLine = [];
      for (var i = 0; i < 5; i++){
        dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: getEmissionsDate(dsVal, deviceDs, now)));
        now = now.subtract(Duration(days:1));

      }

      return dataLine;
    }
    else {
      List<EmissionsLineSeries> dataLine = [];
      for (var i = 0; i < 5; i++){
        dataLine.add(EmissionsLineSeries(date: new DateTime(now.year, now.month, now.day), amounts: 5.479));
        now = now.subtract(Duration(days:1));

      }

      return dataLine;
    }
  }


  // gets the total
  double getTotals(dsVal, deviceDs, type) {

    double sum = 0.0;

    if (dsVal != null) {
      for (var date in dsVal.keys) {
        var value = dsVal[date];
        if (value.containsKey(type)) {
          var entries = value[type];
          for (var num in entries.keys) {

            var values = entries[num];
            if (!(values is int)) {
              if (values.containsKey('duration')) {
                int time = values['duration'].toInt();
                if (values.containsKey('duration')) {
                  int time = values['duration'].toInt();
                  if (values.containsKey('device')) {
                    var device = values['device'];
                    sum += getTotalCO2(time,device, deviceDs, type);
                  }

                }

              }
            }
          }
        }
      }
    }
    return sum;
  }

  }



//bar chart
class EmissionsChart extends StatelessWidget{
  final List<Emissions> data;

  EmissionsChart({@required this.data});

  @override
  Widget build(BuildContext context){

    List<charts.Series<Emissions, String>> series =
        [
          charts.Series(
            id: "Emissions",
            data: data,
            domainFn: (Emissions series,_) => series.emissionType,
            measureFn: (Emissions series, _) => series.amounts,
            colorFn: (Emissions series, _) => series.colour,

          )
        ];
    return Container(
      height: 500,
      padding: EdgeInsets.all(20),
      child: Card(
        child:Padding(
          padding: const EdgeInsets.all(8.0),

          child: Column(
            children: <Widget> [
              Text("Emissions Output Type", style: Theme.of(context).textTheme.body2,
              ),
              Expanded(
                  child: charts.BarChart(
                      series,
                      animate: true,
                      behaviors: [
                        new charts.ChartTitle('Category',
                            titleStyleSpec: charts.TextStyleSpec(fontSize: 14, fontFamily: "Avenir", color: charts.Color.black),
                            behaviorPosition: charts.BehaviorPosition.bottom,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea),
                        new charts.ChartTitle('Kg CO2',
                            titleStyleSpec: charts.TextStyleSpec(fontSize: 14, fontFamily: "Avenir", color: charts.Color.black),
                            behaviorPosition: charts.BehaviorPosition.start,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea)],
                  ),
              )
            ]
          ),
        ),
      ),
    );
  }

}

class Emissions {
  final String emissionType;
  final double amounts;
  final charts.Color colour;

  Emissions({this.emissionType, this.amounts, this.colour});
}


//line graph
class EmissionsLine extends StatelessWidget{
  final List<EmissionsLineSeries> data;
  final List<EmissionsLineSeries> dataAus;
  final String title;

  EmissionsLine({@required this.data, @required this.dataAus, @required this.title });

  @override
  Widget build(BuildContext context){

    List<charts.Series<EmissionsLineSeries, DateTime>> series =
    [
      charts.Series(
        id: "You",
        data: data,
        domainFn: (EmissionsLineSeries series,_) => series.date,
        measureFn: (EmissionsLineSeries series, _) => series.amounts,
        colorFn: (EmissionsLineSeries series, _) => charts.ColorUtil.fromDartColor(Color(0xFF053a54)),

      ),

      charts.Series(
        id: "Recommended",
        data: dataAus,

        domainFn: (EmissionsLineSeries series,_) => series.date,
        measureFn: (EmissionsLineSeries series, _) => series.amounts,
        colorFn: (EmissionsLineSeries series, _) => charts.ColorUtil.fromDartColor(Color(0xFFff9090)),

      )
    ];
    return Container(
      height: 500,
      padding: EdgeInsets.all(20),
      child: Card(
        child:Padding(
          padding: const EdgeInsets.all(8.0),

          child: Column(
              children: <Widget> [
                Text(title, style: Theme.of(context).textTheme.body2,
                ),
                Expanded(
                  child: charts.TimeSeriesChart(series,
                      animate: true,
                      behaviors: [charts.SeriesLegend(),

                        new charts.ChartTitle('Date',
                            titleStyleSpec: charts.TextStyleSpec(fontSize: 14, fontFamily: "Avenir", color: charts.Color.black),
                            behaviorPosition: charts.BehaviorPosition.bottom,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea),
                        new charts.ChartTitle('Kg CO2',
                            titleStyleSpec: charts.TextStyleSpec(fontSize: 14, fontFamily: "Avenir", color: charts.Color.black),
                            behaviorPosition: charts.BehaviorPosition.start,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea)],


                ))
              ]
          ),
        ),
      ),
    );
  }

}

//prediction line graph
class EmissionsPredictorLine extends StatelessWidget{
  final List<EmissionsLineSeries> data;
  final String title;

  EmissionsPredictorLine({@required this.data, @required this.title });

  @override
  Widget build(BuildContext context){

    List<charts.Series<EmissionsLineSeries, DateTime>> series =
    [
      charts.Series(
        id: "Your predicted emissions",
        data: data,
        domainFn: (EmissionsLineSeries series,_) => series.date,
        measureFn: (EmissionsLineSeries series, _) => series.amounts,
        colorFn: (EmissionsLineSeries series, _) => charts.ColorUtil.fromDartColor(Color(0xFF507587)),


      ),

    ];
    return Container(
      height: 500,
      padding: EdgeInsets.all(20),
      child: Card(
        child:Padding(
          padding: const EdgeInsets.all(8.0),

          child: Column(
              children: <Widget> [
                Text(title, style: Theme.of(context).textTheme.body2,
                ),
                Expanded(
                  child: charts.TimeSeriesChart(series,
                      animate: true,
                      behaviors: [charts.SeriesLegend(),
                        new charts.ChartTitle('Date',
                            titleStyleSpec: charts.TextStyleSpec(fontSize: 14, fontFamily: "Avenir", color: charts.Color.black),
                            behaviorPosition: charts.BehaviorPosition.bottom,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea),
                        new charts.ChartTitle('Predicted Kg CO2',
                            titleStyleSpec: charts.TextStyleSpec(fontSize: 14, fontFamily: "Avenir", color: charts.Color.black),
                            behaviorPosition: charts.BehaviorPosition.start,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea)]),
                )
              ]
          ),
        ),
      ),
    );
  }

}

class EmissionsLineSeries {
  final DateTime date;
  final double amounts;
  //final charts.Color colour;

  EmissionsLineSeries({this.date, this.amounts});
}


class Graphs {
  int id;
  String graphType;

  Graphs(this.id, this.graphType);

}

