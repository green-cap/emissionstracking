import 'package:flutter/material.dart';
import 'screens.dart';

class AppScreen extends StatefulWidget {
  @override
  _AppScreenState createState() => _AppScreenState();
}

class _AppScreenState extends State<AppScreen> {
  int _selectedPage = 0;

  final _pageOptions = [
    HomeScreen(),
    EmissionScreen(),
    AddEmissions(),
    DiscoverScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageOptions[_selectedPage],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xFF00263d),
        currentIndex: _selectedPage,
        onTap: (int index) {
          setState(() {
            _selectedPage = index; //Index 0 is login page so increment
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
            backgroundColor: Color(0xFF00263d),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.insert_chart),
            title: Text('Emissions'),
            backgroundColor: Color(0xFF00263d),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_box),
            title: Text('Add'),
            backgroundColor: Color(0xFF00263d),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.explore),
            title: Text('Discover'),
            backgroundColor: Color(0xFF00263d),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
            backgroundColor: Color(0xFF00263d),
          ),
        ],
      ),
    );
  }
}
