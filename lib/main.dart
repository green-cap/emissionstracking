// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'screens/screens.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:async';
import 'dart:io' show Platform;
import 'package:greencap/scheduler/scheduler.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final FirebaseApp app = await Firebase.initializeApp(
    options: Platform.isIOS || Platform.isMacOS
        ? FirebaseOptions(
            appId: '1:248353503754:android:3b7914f5eec87bae327df',
            apiKey: 'AIzaSyBn14iQaln7vOC8k9ZzinIlWy48MGfp3AI',
            projectId: 'greencap-emissions',
            messagingSenderId: '248353503754',
            databaseURL: 'https://greencap-emissions.firebaseio.com/',
          )
        : FirebaseOptions(
            appId: '1:248353503754:android:3b7914f5eec87bae327df3',
            apiKey: 'AIzaSyBn14iQaln7vOC8k9ZzinIlWy48MGfp3AI',
            messagingSenderId: '248353503754',
            projectId: 'greencap-emissions',
            databaseURL: 'https://greencap-emissions.firebaseio.com/',
          ),
  );

  GreenCapScheduler.catchUpWholeSchedule();
  return runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  int _selectedPage = 0;

  final _pageOptions = [
    LoginScreen(),
    AppScreen(),
  ];

  MyAppState() {
    _checkLoggedIn();
  }

  _checkLoggedIn() async {
    final SharedPreferences prefs = await _prefs;
    if (prefs.getBool("isLoggedIn") != null &&
        prefs.getBool("isLoggedIn") == true) {
      setState(() {
        //CHANGE THIS BACK TO 1
        _selectedPage = 1;
      });
    } else {
      setState(() {
        _selectedPage = 0;
      });
    }
  }

  static getTodayDateFormatted() {
    DateTime _date = new DateTime.now();
    return "${_date.day.toString().padLeft(2, '0')}-${_date.month.toString().padLeft(2, '0')}-${_date.year.toString()}";
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Green Cap App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: Scaffold(
        body: _pageOptions[_selectedPage],
      ),
    );
  }
}

/*
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Green Cap App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Color(0xFFF0F2F5),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Dimitri Test App'),
        ),
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.business),
              title: Text('Business'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.school),
              title: Text('School'),
            ),
          ],
        ),
      ),
      //home: HomeScreen(),
    );
  }
}
*/
