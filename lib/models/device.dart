import 'package:meta/meta.dart';

class DeviceK {
  final String name;
  final int id;

  const DeviceK({
    @required this.name,
    @required this.id,
  });
}
